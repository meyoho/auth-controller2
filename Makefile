
# Image URL to use all building/pushing image targets
TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")
IMG ?= index.alauda.cn/alaudak8s/auth-controller2:${TAG}

API_PKG_BASE=bitbucket.org/mathildetech/auth-controller2/pkg/apis

.PHONY: all
all: lint test

# ==============================================================================
# Build Options

ROOT_PACKAGE=bitbucket.org/mathildetech/auth-controller2
VERSION_PACKAGE=bitbucket.org/mathildetech/app/version

# set the shell to bash in case some environments use sh
SHELL := /bin/bash
GO ?= $(shell which go)
PACKAGES ?= $(shell $(GO) list ./...)
GOFILES := $(shell find . -name "*.go" -type f)
GOFMT ?= gofmt "-s"
GO111MODULE=on


# ==============================================================================
# Includes

include build/lib/common.mk
include build/lib/help.mk
include build/lib/golang.mk
include build/lib/image.mk
include build/lib/deploy.mk

# ==============================================================================
# Tasks

.PHONY: build
build:
	@$(MAKE) go.build

# Run tests
test: generate fmt vet manifests
	go test ./pkg/... ./cmd/... -coverprofile cover.out

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager bitbucket.org/mathildetech/auth-controller2/cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: fmt vet
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install: manifests
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: docker-push
#	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true"
manifests:
	@echo "Generating crds for pkg/apis/auth package ..."
	$(CONTROLLER_GEN) $(CRD_OPTIONS) paths="$(API_PKG_BASE)/auth/..." output:dir=./crds

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
generate:
	@echo "Generating deepcopy files for pkg/apis/auth package ..."
	$(CONTROLLER_GEN) object:headerFile=./hack/boilerplate.go.txt paths="$(API_PKG_BASE)/auth/..."

# Build the docker image
docker-build:
	docker build . -t ${IMG}
	@echo "updating kustomize image patch file for manager resource"
	sed -i'' -e 's@image: .*@image: '"${IMG}"'@' ./config/default/manager_image_patch.yaml

# Push the docker image
docker-push: docker-build
	docker push ${IMG}

# find or download controller-gen
# download controller-gen if necessary
controller-gen:
ifeq (, $(shell which controller-gen))
	@{ \
	set -e ;\
	CONTROLLER_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$CONTROLLER_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/controller-tools/cmd/controller-gen@v0.2.1 ;\
	rm -rf $$CONTROLLER_GEN_TMP_DIR ;\
	}
CONTROLLER_GEN=$(GOBIN)/controller-gen
else
CONTROLLER_GEN=$(shell which controller-gen)
endif