/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"bitbucket.org/mathildetech/auth-controller2/pkg/controller"
	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks"
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
)

const (
	hookServerCertDir = "/tmp/k8s-webhook-server/serving-certs"
)

func main() {
	var metricsAddr string
	var labelBaseDomain string
	var syncPeriodTmp int64
	var flagEnableControllers string

	flag.BoolVar(&apis.NeedSyncTke, apis.FlagTkeSwitch, false, "Used to control if sync to tke or not")
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.IntVar(&apis.ProjectWorkers, apis.FlagProjectWorkers, 2, "Num of project reconcile workers")
	flag.IntVar(&apis.ApiTimeout, apis.FlagApiTimeout, 2, "Timeout seconds for request k8s apiserver")
	flag.StringVar(&labelBaseDomain, "label-base-domain", "alauda.io", "The based domain of the resource label.")
	flag.Int64Var(&syncPeriodTmp, "sync-period", 7200, "auth-controller2 sync period.")
	flag.IntVar(&apis.CrdMaxDelaySeconds, apis.FlagCrdMaxDelaySeconds, 5, "Max seconds before quit register controller to manager")
	flag.StringVar(&flagEnableControllers, apis.FlagEnableControllers, "", "define which controller should be enable")
	flag.Parse()

	apis.EnableControllers = strings.Split(flagEnableControllers, ",")

	// NeedSyncTke always be true
	apis.NeedSyncTke = true

	constant.PopulateWithLabelBaseDomain(labelBaseDomain)

	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("auth-controller")

	// Serve a health check.
	healthMux := http.NewServeMux()
	healthMux.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	go func() {
		err := http.ListenAndServe(":8081", healthMux)
		if err != nil {
			log.Error(err, "Health serving failed")
		}
	}()

	// Get a config to talk to the apiserver
	log.Info("setting up client for manager")
	cfg, err := config.GetConfig()
	if err != nil {
		log.Error(err, "unable to set up client config")
		os.Exit(1)
	}

	// Create a new Cmd to provide shared dependencies and start components
	leaderElectionNamespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	log.Info("setting up manager", "leader election namespace", leaderElectionNamespace)
	syncPeriod := time.Duration(syncPeriodTmp) * time.Second
	mgr, err := manager.New(cfg, manager.Options{
		LeaderElection:          true,
		LeaderElectionID:        "auth-controller-lock",
		LeaderElectionNamespace: leaderElectionNamespace,
		MetricsBindAddress:      metricsAddr,
		SyncPeriod:              &syncPeriod,
		Port:                    9443,
		CertDir:                 hookServerCertDir,
	})

	if err != nil {
		log.Error(err, "unable to set up overall controller manager")
		os.Exit(1)
	}

	// registry scheme
	v1beta1.AddToScheme(mgr.GetScheme())

	// Note: if kind is a CRD, it should be installed before calling Start!
	// init the project
	//if err := project.InitDefaultProject(mgr.GetClient()); err != nil {
	//	log.Error(err, "Init project error")
	//}

	// init the default ClusterRoles
	//mgr.Add(manager.RunnableFunc(func(s <-chan struct{}) error {
	//	if err := clusterrole.InitDefaultClusterRoles(mgr.GetClient()); err != nil {
	//		log.Error(err, "Init default clusterroles error")
	//	}
	//	<-s
	//	return err
	//}))

	log.Info("Registering Components.")

	// Setup Scheme for all resources
	log.Info("setting up scheme")
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Error(err, "unable add APIs to scheme")
		os.Exit(1)
	}

	// Setup all Controllers
	log.Info("Setting up controller")
	if err := controller.AddToManager(mgr); err != nil {
		log.Error(err, "unable to register controllers to the manager")
		os.Exit(1)
	}

	// Setup Webhooks
	log.Info("setting up webhook server")
	hookServer := mgr.GetWebhookServer()
	webhooks.RegisterWebhooks(hookServer)

	// Start the Cmd
	log.Info("Starting the Cmd.")
	if err := mgr.Start(signals.SetupSignalHandler()); err != nil {
		log.Error(err, "unable to run the manager")
		os.Exit(1)
	}
}
