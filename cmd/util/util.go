package util

import (
	"crypto/md5"
	"encoding/hex"
	"hash/fnv"
	"os"
	"strconv"
)

// GetEnv get value from os environment, if not available, then return fallback.
func GetEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if exists && len(value) != 0 {
		return value
	}
	return fallback
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func HashStr(s string) string {
	h := fnv.New32a()
	h.Write([]byte(s))
	return strconv.FormatInt(int64(h.Sum32()), 10)
}
