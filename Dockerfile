# Copyright 2017 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
FROM alpine:3.11 as builder

RUN apk add --no-cache upx

COPY ./output  /opt/output

WORKDIR /opt/output

# pick the architecture
RUN ARCH= && dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) ARCH='amd64';; \
    aarch64) ARCH='arm64';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && cp linux/${ARCH}/manager manager \
  && upx manager

FROM golang:1.13-alpine

RUN sed -i 's|dl-cdn.alpinelinux.org|mirrors.aliyun.com|g' /etc/apk/repositories
RUN apk add --no-cache ca-certificates

COPY --from=builder /opt/output/manager /manager
ENTRYPOINT ["/manager"]

ARG commit_id=dev
ARG app_version=dev
ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
