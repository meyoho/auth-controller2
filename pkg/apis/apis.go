/*
Copyright 2018 The Kubernetes.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis contains Kubernetes API groups.
package apis

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"

	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	apiextension "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

const (
	FlagTkeSwitch          string = "tke"
	FlagProjectWorkers     string = "project-workers"
	FlagApiTimeout         string = "api-timeout"
	FlagCrdMaxDelaySeconds string = "crd-max-delay-seconds"
	FlagEnableControllers  string = "enable-controllers"
	Cluster                string = "clusters"
	ClusterRole            string = "clusterroles"
	Connector              string = "connectors"
	FunctionResource       string = "functionresources"
	Project                string = "projects"
	ProjectBinding         string = "projectbindings"
	RoleTemplate           string = "roletemplates"
	User                   string = "users"
	UserBinding            string = "userbindings"
)

var NeedSyncTke bool
var ProjectWorkers int
var ApiTimeout int
var CrdMaxDelaySeconds int
var EnableControllers []string

var log = logf.Log.WithName("apis")

// AddToSchemes may be used to add all resources defined in the project to a Scheme
var AddToSchemes runtime.SchemeBuilder

// AddToScheme adds all Resources to the Scheme
func AddToScheme(s *runtime.Scheme) error {
	return AddToSchemes.AddToScheme(s)
}

type ResourceObject struct {
	Object runtime.Object
	Name   string
}

func InitResource(k8sClient client.Client, resourceObject ResourceObject) error {

	log.Info("Create resource", "named", resourceObject.Name)
	if err := k8sClient.Create(context.TODO(), resourceObject.Object); err != nil && !errors.IsAlreadyExists(err) {
		log.Error(err, "creating projects")
		return err
	}
	return nil
}

func NewClientForCluster(cluster, token string) (client.Client, error) {
	config := &rest.Config{
		Host:        GetErebusEndpoint() + "/kubernetes/" + cluster,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	return client.New(config, client.Options{})
}

func NewExtenionClientForCluster(cluster, token string) (*apiextension.Clientset, error) {
	config := &rest.Config{
		Host:        GetErebusEndpoint() + "/kubernetes/" + cluster,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	return apiextension.NewForConfig(config)
}

// sync resource to clusters
func SyncClusters(client client.Client, filterClusters []string, obj runtime.Object, f func(clusterClient client.Client, clusterName string, obj runtime.Object)) {
	clusters := clusterv1alpha1.ListClusters(client)
	//log.Info("SyncClusters", "clusters", clusters)
Loop:
	for _, cluster := range clusters {

		// check cluster status
		for _, condition := range cluster.Status.Conditions {
			if condition.Type == clusterv1alpha1.NotAccessible && condition.Status == corev1.ConditionTrue {
				log.Info("Cluster status NotAccessible is true", "cluster", cluster.Name)
				continue Loop
			}
		}

		//clusterName, token, err := cluster.GetClusterNameAndToken()
		clusterName, token, err := cluster.GetClusterNameAndToken2(client)
		if err != nil {
			log.Error(err, "SyncClusters get cluster token", "cluster", cluster.Name)
			continue
		}

		if len(filterClusters) > 0 && !util.StringInSlice(clusterName, filterClusters) {
			continue
		}

		cli, err := NewClientForCluster(clusterName, token)
		if err != nil {
			if !errors.IsNotFound(err) && !strings.Contains(err.Error(), "not found") {
				log.Error(err, "NewClientForCluster", "cluster", clusterName)
			}
			continue
		}

		go f(cli, clusterName, obj)
	}
}

func IsRegisteredCrd(mgr manager.Manager, obj runtime.Object) bool {
	err := mgr.GetClient().Get(context.TODO(), types.NamespacedName{Name: "test"}, obj)
	if err != nil && strings.Contains(err.Error(), "no matches for kind") {
		return false
	}
	return true
}

// create rolebinding
func CreateRoleBinding(cli client.Client, scheme *runtime.Scheme, instance *authv1.UserBinding, namespace string, roleName string) (err error) {
	log.Info("CreateRoleBindings start:", "namespace:", namespace, "roleName:", roleName, "userBinding:", instance.Name)
	instanceName := strings.ToLower(instance.Name)
	name := fmt.Sprintf("%s-%s", roleName, instanceName)
	if len(namespace) > 0 {
		name = fmt.Sprintf("%s-%s-%s", roleName, namespace, instanceName)
	}
	userEmail := instance.UserEmail()
	rolebinding := &rbacv1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		RoleRef: rbacv1.RoleRef{
			APIGroup: rbacv1.GroupName,
			Kind:     "ClusterRole",
			Name:     roleName,
		},
		Subjects: []rbacv1.Subject{
			{
				APIGroup: rbacv1.GroupName,
				Kind:     rbacv1.UserKind,
				Name:     userEmail,
			},
		},
	}
	rolebinding.SetLabels(map[string]string{
		rc.LabelUserBindingName: instance.Name,
	})

	if err = controllerutil.SetControllerReference(instance, rolebinding, scheme); err != nil {
		return
	}

	// Check if the RoleBinding already exists
	if err = cli.Create(context.TODO(), rolebinding); err != nil && !errors.IsAlreadyExists(err) {
		return
	}
	log.Info("CreateRoleBindings success:", "namespace:", namespace, "roleName:", roleName, "userBinding:", instance.Name)
	return nil
}

// create clusterrolebinding
func CreateClusterRoleBinding(cli client.Client, scheme *runtime.Scheme, instance *authv1.UserBinding, namespace string, roleName string) (err error) {
	instanceName := strings.ToLower(instance.Name)
	name := fmt.Sprintf("%s-%s", roleName, instanceName)
	if len(namespace) > 0 {
		name = fmt.Sprintf("%s-%s-%s", roleName, namespace, instanceName)
	}
	userEmail := instance.UserEmail()
	clusterRoleBinding := &rbacv1.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		RoleRef: rbacv1.RoleRef{
			APIGroup: rbacv1.GroupName,
			Kind:     "ClusterRole",
			Name:     roleName,
		},
		Subjects: []rbacv1.Subject{
			{
				APIGroup: rbacv1.GroupName,
				Kind:     rbacv1.UserKind,
				Name:     userEmail,
			},
		},
	}
	clusterRoleBinding.SetLabels(map[string]string{
		rc.LabelUserBindingName: instance.Name,
	})

	if err = controllerutil.SetControllerReference(instance, clusterRoleBinding, scheme); err != nil {
		return
	}

	// Check if the ClusterRoleBinding already exists
	if err = cli.Create(context.TODO(), clusterRoleBinding); err != nil && !errors.IsAlreadyExists(err) {
		return
	}
	return nil
}

func IsSystemRole(role *rbacv1.ClusterRole) bool {
	var (
		rolePart string
		ok       bool
	)
	if rolePart, ok = role.Labels[rc.LabelRolePart]; !ok {
		return false
	}
	if rolePart == rc.RoleLabelPartSystem {
		return true
	}
	return false
}

func IsCommonRole(role *rbacv1.ClusterRole) bool {
	var (
		rolePart string
		ok       bool
	)
	if rolePart, ok = role.Labels[rc.LabelRolePart]; !ok {
		return false
	}
	if rolePart == rc.RoleLabelPartCommon {
		return true
	}
	return false
}

func IsSystemGlobalRole(role *rbacv1.ClusterRole) bool {
	var (
		currentCluster string
		rolePart       string
		bindCluster    string
		ok             bool
	)
	if currentCluster, ok = role.Annotations[rc.AnnotationCurrentCluster]; !ok {
		return false
	}
	if rolePart, ok = role.Labels[rc.LabelRolePart]; !ok {
		return false
	}
	if bindCluster, ok = role.Labels[rc.LabelRoleBindCluster]; !ok {
		return false
	}
	if currentCluster == rc.ClusterGlobal &&
		rolePart == rc.RoleLabelPartSystem &&
		bindCluster == rc.RoleBindClusterGlobal {
		return true
	}
	return false
}

func IsSystemBusinessRole(role *rbacv1.ClusterRole) bool {
	var (
		rolePart    string
		bindCluster string
		ok          bool
	)
	if rolePart, ok = role.Labels[rc.LabelRolePart]; !ok {
		return false
	}
	if bindCluster, ok = role.Labels[rc.LabelRoleBindCluster]; !ok {
		return false
	}
	if rolePart == rc.RoleLabelPartSystem && bindCluster == rc.RoleBindClusterBusiness {
		return true
	}
	return false
}

func IsGlobalProjectNamespaceRole(role *rbacv1.ClusterRole) bool {
	var (
		currentCluster string
		rolePart       string
		ok             bool
	)
	if currentCluster, ok = role.Annotations[rc.AnnotationCurrentCluster]; !ok {
		return false
	}
	if rolePart, ok = role.Labels[rc.LabelRolePart]; !ok {
		return false
	}
	if currentCluster == rc.ClusterGlobal && rolePart == rc.RoleLabelPartProjectNs {
		return true
	}
	return false
}

func GetRoleBindScope(role *rbacv1.ClusterRole) string {
	if bindscope, ok := role.Labels[rc.LabelRoleBindScope]; ok {
		return bindscope
	}
	return ""
}

func IsGlobalClusterRole(role *rbacv1.ClusterRole) bool {
	if cluster, ok := role.Annotations[rc.AnnotationCurrentCluster]; ok && cluster == rc.ClusterGlobal {
		return true
	}
	return false
}

func IsPlatformGenerateRole(role *rbacv1.ClusterRole) bool {
	if _, ok := role.Annotations[rc.AnnotationCurrentCluster]; !ok {
		return false
	}
	if _, ok := role.Labels[rc.LabelRoleBindCluster]; !ok {
		return false
	}
	if _, ok := role.Labels[rc.LabelRoleBindScope]; !ok {
		return false
	}
	if _, ok := role.Labels[rc.LabelRoleLevel]; !ok {
		return false
	}
	if _, ok := role.Labels[rc.LabelRoleOfficial]; !ok {
		return false
	}
	if _, ok := role.Labels[rc.LabelRoleRelative]; !ok {
		return false
	}
	return true
}

func IsGlobalClusterRoleAndPlatformGenerateRole(role *rbacv1.ClusterRole) bool {
	if IsGlobalClusterRole(role) && IsPlatformGenerateRole(role) {
		return true
	}
	return false

}

func IsProjectSameNameRole(role *rbacv1.ClusterRole) bool {
	var (
		rolePart string
		ok       bool
	)
	if rolePart, ok = role.Labels[rc.LabelRolePart]; !ok {
		return false
	}
	if rolePart == rc.RoleLabelPartProjectNs {
		return true
	}
	return false
}

func ClusterRoleForBusinessCluster(role *rbacv1.ClusterRole) *rbacv1.ClusterRole {
	annotations := role.GetAnnotations()
	if len(annotations) == 0 {
		annotations = make(map[string]string, 0)
	}
	annotations[rc.AnnotationCurrentCluster] = rc.ClusterBusiness
	return &rbacv1.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{
			Name:        role.Name,
			Labels:      role.GetLabels(),
			Annotations: annotations,
		},
		Rules: role.Rules,
	}
}

func GetGlobalUserBindingList(cli client.Client, project string) (*authv1.UserBindingList, error) {

	list := &authv1.UserBindingList{}
	var labelSelectorStr string
	if len(project) > 0 {
		labelSelectorStr = fmt.Sprintf("%s=%s,%s=%s", rc.LabelRoleLevel, rc.RoleLevelProject, rc.LabelProject, project)
	} else {
		labelSelectorStr = fmt.Sprintf("%s=%s", rc.LabelRoleLevel, rc.RoleLevelPlatform)
	}
	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		return nil, err
	}

	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	if err := cli.List(context.TODO(), list, opts); err != nil {
		return nil, err
	}
	return list, nil
}

func GetGlobalClusterRoles(cli client.Client) (*rbacv1.ClusterRoleList, error) {

	list := &rbacv1.ClusterRoleList{}
	labelSelectorStr := fmt.Sprintf("%s,%s", rc.LabelRoleRelative, rc.LabelRoleBindScope)
	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		return nil, err
	}

	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	if err := cli.List(context.TODO(), list, opts); err != nil {
		return nil, err
	}
	return list, nil
}

// GetErebusEndpoint get the EREBUS_ENDPOINT env if exist, otherwise default to
// "https://erebus.{{ LEADER_ELECTION_NAMESPACE }}.svc.cluster.local"
func GetErebusEndpoint() string {
	erebusNamespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	erebusEndpoint := fmt.Sprintf("%s.%s.%s", "https://erebus", erebusNamespace, "svc.cluster.local")
	return util.GetEnv("EREBUS_ENDPOINT", erebusEndpoint)
}

// NeedSyncTkeProject will query environment and answer if need to sync aladua project to tke or not
func NeedSyncTkeProject() bool {
	return NeedSyncTke
}

func IsResourceEnabled(controllerName string) bool {
	if EnableControllers[0] == "*" {
		return true
	}
	return ContainsString(EnableControllers, controllerName)
}

func ContainsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}
