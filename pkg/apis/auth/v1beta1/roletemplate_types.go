/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	v1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// RoleTemplateSpec defines the desired state of RoleTemplate
type RoleTemplateSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Rules       []Rule       `json:"rules"`
	CustomRules []CustomRule `json:"customRules"`
}

type Rule struct {
	Module              string       `json:"module"`
	FunctionResourceRef string       `json:"functionResourceRef"`
	Verbs               metav1.Verbs `json:"verbs"`
}

type CustomRule struct {
	ApiGroup  string       `json:"apiGroup"`
	Resources []string     `json:"resources"`
	Verbs     metav1.Verbs `json:"verbs"`
}

// RoleTemplateStatus defines the observed state of RoleTemplate
type RoleTemplateStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// RoleTemplate is the Schema for the roletemplates API
// +k8s:openapi-gen=true
type RoleTemplate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RoleTemplateSpec   `json:"spec,omitempty"`
	Status RoleTemplateStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// RoleTemplateList contains a list of RoleTemplate
type RoleTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []RoleTemplate `json:"items"`
}

func init() {
	SchemeBuilder.Register(&RoleTemplate{}, &RoleTemplateList{})
}

func (u *RoleTemplate) Validate() error {
	flag := false
	for _, v := range []string{"project", "platform", "namespace"} {
		if v == u.RoleLevel() {
			flag = true
			break
		}
	}
	if flag == false {
		return fmt.Errorf("roletemplate level label is invalid")
	}
	return nil
}

func (u *RoleTemplate) RoleLevel() string {
	if level, ok := u.Labels[rc.LabelRoleTemplateLevel]; ok {
		return level
	}
	return ""
}

func (u *RoleTemplate) Official() string {
	if official, ok := u.Labels[rc.LabelRoleTemplateOfficial]; ok {
		return official
	}
	return ""
}

func (u *RoleTemplate) DisplayName() string {
	if displayName, ok := u.Annotations[rc.AnnotationDisplayName]; ok {
		return displayName
	}
	return ""
}

func (u *RoleTemplate) IsOfficial() bool {
	v, err := strconv.ParseBool(u.Official())
	if err != nil {
		return false
	}
	return v
}

func (u *RoleTemplate) CreatorEmail() string {
	if email, ok := u.Labels[rc.LabelCreatorEmail]; ok {
		return email
	}
	return ""
}

func (u *RoleTemplate) IsPlatformRule() bool {
	if u.Spec.Rules != nil {
		for _, rule := range u.Spec.Rules {
			if rule.Module == "*" && rule.FunctionResourceRef == "*" {
				return true
			}
		}
	}
	return false
}

func (u *RoleTemplate) SetCurrentCluster(cluster string) {
	if len(u.GetAnnotations()) == 0 {
		u.Annotations = make(map[string]string, 0)
	}
	u.Annotations[rc.AnnotationCurrentCluster] = cluster
}

func (u *RoleTemplate) IsNeedUpdateLabel() bool {
	if u.Spec.Rules != nil {
		for _, rule := range u.Spec.Rules {
			if rule.Module == "*" && rule.FunctionResourceRef == "*" {
				continue
			}
			labelName := fmt.Sprintf(rc.LabelSchemaFunctionResourceRef,
				rule.Module, rule.FunctionResourceRef)
			if value, ok := u.ObjectMeta.Labels[labelName]; !ok {
				return true
			} else if value == "false" {
				return true
			}
		}
	}
	if value, ok := u.Labels[rc.LabelRoleTemplateName]; !ok || value != u.Name {
		return true
	}
	return false
}

func (u *RoleTemplate) MaintainFunctionResourceRefLabelConsistent() {
	if u.ObjectMeta.Labels != nil {
		for k := range u.ObjectMeta.Labels {
			if strings.HasPrefix(k, rc.LabelFunctionResourceRef) {
				u.ObjectMeta.Labels[k] = "false"
			}
		}
	}
	if u.Spec.Rules != nil {
		for _, rule := range u.Spec.Rules {
			if rule.Module == "*" && rule.FunctionResourceRef == "*" {
				continue
			}
			labelName := fmt.Sprintf(rc.LabelSchemaFunctionResourceRef,
				rule.Module, rule.FunctionResourceRef)
			u.ObjectMeta.Labels[labelName] = "true"
		}
	}
	// update label name for query
	u.Labels[rc.LabelRoleTemplateName] = u.Name
}

func (u *RoleTemplate) InitRoleMap(roleMap map[string]v1.ClusterRole, roleType, roleSuffix string) {
	if u.RoleLevel() == rc.RoleLevelPlatform {
		roleName := fmt.Sprintf(rc.ClusterRoleNameSchemaCustomer, roleType, roleSuffix)
		clusterRole := v1.ClusterRole{}
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeCluster,
				rc.LabelRoleBindCluster: rc.RoleBindClusterUnlimit,
				rc.LabelRolePart:        "",
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
	} else {
		roleName := fmt.Sprintf(rc.ClusterRoleNameSchemaCustomer, roleType, roleSuffix)
		clusterRole := v1.ClusterRole{}
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeNamespace,
				rc.LabelRoleBindCluster: rc.RoleBindClusterUnlimit,
				rc.LabelRolePart:        rc.RoleLabelPartCommon,
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
		clusterRole = v1.ClusterRole{}
		roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSystem, roleType, roleSuffix)
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeNamespace,
				rc.LabelRoleBindCluster: rc.RoleBindClusterGlobal,
				rc.LabelRolePart:        rc.RoleLabelPartSystem,
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
		clusterRole = v1.ClusterRole{}
		roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSystemBusiness, roleType, roleSuffix)
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeNamespace,
				rc.LabelRoleBindCluster: rc.RoleBindClusterBusiness,
				rc.LabelRolePart:        rc.RoleLabelPartSystem,
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
		clusterRole = v1.ClusterRole{}
		roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaProject, roleType, roleSuffix)
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeNamespace,
				rc.LabelRoleBindCluster: rc.RoleBindClusterGlobal,
				rc.LabelRolePart:        rc.RoleLabelPartProjectNs,
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
		clusterRole = v1.ClusterRole{}
		roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaCluster, roleType, roleSuffix)
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeCluster,
				rc.LabelRoleBindCluster: rc.RoleBindClusterUnlimit,
				rc.LabelRolePart:        "",
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
		clusterRole = v1.ClusterRole{}
		roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSpecificBusiness, roleType,
			"kube-public", roleSuffix)
		FillClusterRole(
			&clusterRole,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeNamespace,
				rc.LabelRoleBindCluster: rc.RoleBindClusterBusiness,
				rc.LabelRolePart:        "kube-public",
				rc.LabelRoleRelative:    u.Name,
				rc.LabelRoleLevel:       u.RoleLevel(),
				rc.LabelRoleOfficial:    u.Official(),
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: u.DisplayName(),
			})
		roleMap[roleName] = clusterRole
	}
}

func (u *RoleTemplate) ConvertToClusterRoles(cli client.Client) map[string]v1.ClusterRole {
	//jsonStr, _ := json.MarshalIndent(u, "", "    ")
	//log.Infof("RoleTemplate:%s", string(jsonStr))
	roleMap := map[string]v1.ClusterRole{}
	var roleType, roleSuffix string
	if u.IsOfficial() {
		roleType = "acp"
		roleSuffix = strings.TrimPrefix(u.Name, "acp-")
	} else {
		roleType = "custom"
		roleSuffix = u.Name
	}
	u.InitRoleMap(roleMap, roleType, roleSuffix)
	if u.Spec.Rules != nil {
		for _, v := range u.Spec.Rules {
			if v.Module == "*" && v.FunctionResourceRef == "*" {
				UpdatePlatformClusterRole(roleMap, u.Name, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
				continue
			}
			frName := v.Module + "-" + v.FunctionResourceRef
			fr, err := GetFunctionResource(cli, frName)
			if err != nil {
				continue
			}
			if fr.Spec.Rules != nil {
				for _, frRule := range fr.Spec.Rules {
					var roleName string
					// platform level
					if u.RoleLevel() == rc.RoleLevelPlatform {
						roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaCustomer, roleType, roleSuffix)
						UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterUnlimit,
							frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
						continue
					}
					// project and namespace level
					switch frRule.BindScope {
					case rc.RoleBindScopeCluster:
						{
							roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaCluster, roleType, roleSuffix)
							UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterUnlimit,
								frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
						}
					case rc.RoleBindScopeNamespace:
						{
							switch frRule.BindNamespacePart {
							case rc.RoleLabelPartSystem:
								{
									if frRule.BindCluster == rc.RoleBindClusterGlobal {
										roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSystem, roleType, roleSuffix)
										UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterGlobal,
											frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
									} else if frRule.BindCluster == rc.RoleBindClusterBusiness ||
										frRule.BindCluster == rc.RoleBindClusterUnlimit {
										roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSystem, roleType, roleSuffix)
										UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterGlobal,
											frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
										roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSystemBusiness, roleType, roleSuffix)
										UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterBusiness,
											frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
									}
								}
							case rc.RoleLabelPartProjectNs:
								{
									roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaProject, roleType, roleSuffix)
									UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterGlobal,
										frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
								}
							case rc.RoleLabelPartCommon:
								{
									roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaCustomer, roleType, roleSuffix)
									UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterUnlimit,
										frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
								}
							default:
								{
									if frRule.BindNamespacePart != "" {
										roleName = fmt.Sprintf(rc.ClusterRoleNameSchemaSpecificBusiness, roleType,
											frRule.BindNamespacePart, roleSuffix)
										UpdateRoleMap(roleMap, roleName, u.Name, rc.RoleBindClusterBusiness,
											frRule, v.Verbs, u.RoleLevel(), u.Official(), u.DisplayName())
									}
								}
							}
						}
					}
				}
			}
		}
	}
	if u.RoleLevel() == rc.RoleLevelPlatform {
		// platform level
		roleName := fmt.Sprintf(rc.ClusterRoleNameSchemaCustomer, roleType, roleSuffix)
		UpdateRoleMapCustomRules(roleMap, roleName, u.Spec.CustomRules, rc.RoleBindScopeCluster, rc.RoleBindClusterUnlimit,
			"", u.Name, u.RoleLevel(), u.Official(), u.DisplayName())
	} else {
		roleName := fmt.Sprintf(rc.ClusterRoleNameSchemaCustomer, roleType, roleSuffix)
		UpdateRoleMapCustomRules(roleMap, roleName, u.Spec.CustomRules, rc.RoleBindScopeNamespace, rc.RoleBindClusterUnlimit,
			rc.RoleLabelPartCommon, u.Name, u.RoleLevel(), u.Official(), u.DisplayName())
	}
	return roleMap
}

func (r *RoleTemplate) AddExtraDataForClusterRole(clusterRole *v1.ClusterRole) {
	// add ownerReference
	r.SetCurrentCluster(rc.RoleBindClusterGlobal)
	owner := make([]metav1.OwnerReference, 0)
	owner = append(owner, metav1.OwnerReference{
		APIVersion: SchemeGroupVersion.String(),
		Kind:       "RoleTemplate",
		Name:       r.Name,
		UID:        r.UID,
	})
	clusterRole.SetOwnerReferences(owner)
}

func UpdateRoleMapCustomRules(roleMap map[string]v1.ClusterRole, roleName string, customRules []CustomRule,
	bindScope, bindCluster, bindNamespacePart,
	relative, roleLevel, official, displayName string) {
	role := v1.ClusterRole{}
	if r, ok := roleMap[roleName]; !ok {
		FillClusterRole(
			&role,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   bindScope,
				rc.LabelRoleBindCluster: bindCluster,
				rc.LabelRolePart:        bindNamespacePart,
				rc.LabelRoleRelative:    relative,
				rc.LabelRoleLevel:       roleLevel,
				rc.LabelRoleOfficial:    official,
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: displayName,
			})
	} else {
		role = r
	}
	for _, rule := range customRules {
		role.Rules = append(role.Rules, v1.PolicyRule{
			APIGroups: []string{rule.ApiGroup},
			Resources: rule.Resources,
			Verbs:     rule.Verbs,
		})
	}
	roleMap[roleName] = role
}

func UpdateRoleMap(roleMap map[string]v1.ClusterRole, roleName, relative, bindCluster string,
	frRule FRRule, verbs metav1.Verbs, roleLevel, official, displayName string) {
	role := v1.ClusterRole{}
	if r, ok := roleMap[roleName]; !ok {
		FillClusterRole(
			&role,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   frRule.BindScope,
				rc.LabelRoleBindCluster: bindCluster,
				rc.LabelRolePart:        frRule.BindNamespacePart,
				rc.LabelRoleRelative:    relative,
				rc.LabelRoleLevel:       roleLevel,
				rc.LabelRoleOfficial:    official,
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: displayName,
			})
	} else {
		role = r
	}
	policyRule := v1.PolicyRule{}
	apiGroups := []string{frRule.ApiGroup}
	policyRule.APIGroups = apiGroups
	policyRule.Resources = frRule.Resources
	policyRule.ResourceNames = frRule.ResourceNames
	policyRule.Verbs = verbs
	policyRule.NonResourceURLs = frRule.NonResourceURLs
	role.Rules = append(role.Rules, policyRule)
	roleMap[roleName] = role
}

func (r *RoleTemplate) GetReferenceClusterRoleList(cli client.Client) (*v1.ClusterRoleList, error) {
	ls, err := labels.Parse(fmt.Sprintf("%s,%s=%s", rc.LabelRoleRelative, rc.LabelRoleRelative, r.Name))
	if err != nil {
		return nil, err
	}
	opts := &client.ListOptions{
		LabelSelector: ls,
	}
	list := &v1.ClusterRoleList{}

	if err := cli.List(context.TODO(), list, opts); err != nil {
		return nil, err
	}
	return list, nil
}

func FillClusterRole(role *v1.ClusterRole, roleName string, labels, annotations map[string]string) {
	role.TypeMeta = metav1.TypeMeta{}
	role.TypeMeta.APIVersion = "rbac.authorization.k8s.io/v1"
	role.TypeMeta.Kind = "ClusterRole"
	role.ObjectMeta = metav1.ObjectMeta{}
	role.ObjectMeta.Labels = map[string]string{}
	role.ObjectMeta.Annotations = map[string]string{}
	role.ObjectMeta.Name = roleName
	for k, v := range labels {
		role.ObjectMeta.Labels[k] = v
	}
	annotations[rc.AnnotationCurrentCluster] = rc.ClusterGlobal
	role.ObjectMeta.Annotations = annotations
}

func UpdatePlatformClusterRole(roleMap map[string]v1.ClusterRole, roleName string, verbs []string, level, official, displayName string) {
	role := v1.ClusterRole{}
	if r, ok := roleMap[roleName]; !ok {
		FillClusterRole(
			&role,
			roleName,
			map[string]string{
				rc.LabelRoleBindScope:   rc.RoleBindScopeCluster,
				rc.LabelRoleBindCluster: rc.RoleBindClusterUnlimit,
				rc.LabelRolePart:        "",
				rc.LabelRoleRelative:    roleName,
				rc.LabelRoleLevel:       level,
				rc.LabelRoleOfficial:    official,
				rc.LabelRoleVisible:     "false",
			},
			map[string]string{
				rc.AnnotationDisplayName: displayName,
			})
	} else {
		role = r
	}
	role.Rules = []v1.PolicyRule{
		{
			APIGroups: []string{"*"},
			Resources: []string{"*"},
			Verbs:     verbs},
		{
			NonResourceURLs: []string{"*"},
			Verbs:           verbs}}
	roleMap[roleName] = role
}
