/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// FunctionResourceSpec defines the desired state of FunctionResource
type FunctionResourceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Rules []FRRule `json:"rules"`
}

type FRRule struct {
	ApiGroup          string   `json:"apiGroup"`
	Resources         []string `json:"resources"`
	ResourceNames     []string `json:"resourceNames,omitempty"`
	NonResourceURLs   []string `json:"nonResourceURLs,omitempty"`
	BindScope         string   `json:"bindScope"`
	BindCluster       string   `json:"bindCluster"`
	BindNamespacePart string   `json:"bindNamespacePart"`
}

// FunctionResourceStatus defines the observed state of FunctionResource
type FunctionResourceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// FunctionResource is the Schema for the functionresources API
// +k8s:openapi-gen=true
type FunctionResource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   FunctionResourceSpec   `json:"spec,omitempty"`
	Status FunctionResourceStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// FunctionResourceList contains a list of FunctionResource
type FunctionResourceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []FunctionResource `json:"items"`
}

func init() {
	SchemeBuilder.Register(&FunctionResource{}, &FunctionResourceList{})
}

func GetFunctionResource(cli client.Client, name string) (*FunctionResource, error) {
	fr := &FunctionResource{}
	err := cli.Get(context.TODO(),
		types.NamespacedName{
			Name: name,
		},
		fr)
	if err != nil {
		return nil, err
	}
	return fr, nil
}
