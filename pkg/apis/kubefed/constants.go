package kubefed

const (
	NamespaceName = "namespaces"
	NamespaceKind = "Namespace"

	ServiceKind = "Service"

	ServiceAccountKind = "ServiceAccount"
	// ServiceAccount fields
	SecretsField = "secrets"
	SpecField    = "spec"
	// Template fields
	TemplateField        = "template"
	PlacementField       = "placement"
	ClusterSelectorField = "clusterSelector"
	MatchLabelsField     = "matchLabels"
)
