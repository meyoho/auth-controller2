package kubefed

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type UserBindingSpec map[string]interface{}

// FederatedServiceClusterStatus is the observed status of the resource for a named cluster
type FederatedUserBinding struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec FederatedUserBindingSpec `json:"spec"`
	// +optional
	Status FederatedUserBindingStatus `json:"status"`
}

// UserBindingStatus defines the observed state of UserBinding
type FederatedUserBindingStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}
type FederatedUserBindingSpec struct {
	UserBindingSpec `json:"spec,omitempty"`
}

// +kubebuilFder:object:root=true

// FederatedServiceStatusList contains a list of FederatedServiceStatus
type FederatedUserBindingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []FederatedUserBinding `json:"items"`
}

func init() {
	SchemeBuilder.Register(&FederatedUserBinding{}, &FederatedUserBindingList{})
}
