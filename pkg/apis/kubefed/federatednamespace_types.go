package kubefed

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type NamespaceSpec map[string]interface{}

// FederatedNamespace is the observed status of the resource for a named cluster
type FederatedNamespace struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec FederatedNamespaceSpec `json:"spec"`
	// +optional
	Status FederatedNamespaceStatus `json:"status"`
}

// FederatedNamespaceStatus defines the observed state of FederatedNamespace
type FederatedNamespaceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}
type FederatedNamespaceSpec struct {
	NamespaceSpec `json:"spec,omitempty"`
}

// +kubebuilFder:object:root=true

// FederatedNamespaceList contains a list of FederatedNamespace
type FederatedNamespaceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []FederatedNamespace `json:"items"`
}

func init() {
	SchemeBuilder.Register(&FederatedNamespace{}, &FederatedNamespaceList{})
}
