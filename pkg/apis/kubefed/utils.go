package kubefed

import (
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/kubefed/typeconfig"
	"fmt"
	"github.com/pkg/errors"
	apiextv1b1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func FederateResources(resources *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	gvk := resources.GroupVersionKind()

	// Construct an API Resource from above info.
	// TODO(irfanurrehman) Should we depend on the lookup from the
	// API Server instead, for some specific scenario?
	plural, singular := apimeta.UnsafeGuessKindToResource(gvk)
	apiResource := metav1.APIResource{
		Name:         plural.Resource,
		SingularName: singular.Resource,
		Group:        gvk.Group,
		Version:      gvk.Version,
		Kind:         gvk.Kind,
	}
	apiResource.Namespaced = resources.GetNamespace() == ""
	qualifiedName := NewQualifiedName(resources)
	typeConfig := GenerateTypeConfigForTarget(apiResource, NewEnableTypeDirective())
	federatedResource, err := FederatedResourceFromTargetResource(typeConfig, resources)
	if err != nil {
		return nil, errors.Wrapf(err, "Error getting %s from %s %q", typeConfig.GetFederatedType().Kind, typeConfig.GetTargetType().Kind, qualifiedName)
	}

	return federatedResource, nil
}

func GenerateTypeConfigForTarget(apiResource metav1.APIResource, enableTypeDirective *EnableTypeDirective) typeconfig.Interface {
	spec := enableTypeDirective.Spec
	kind := apiResource.Kind
	pluralName := apiResource.Name
	typeConfig := &FederatedTypeConfig{
		// Explicitly including TypeMeta will ensure it will be
		// serialized properly to yaml.
		TypeMeta: metav1.TypeMeta{
			Kind:       "FederatedTypeConfig",
			APIVersion: "core.kubefed.io/v1beta1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: typeconfig.GroupQualifiedName(apiResource),
		},
		Spec: FederatedTypeConfigSpec{
			TargetType: APIResource{
				Version: apiResource.Version,
				Kind:    kind,
				Scope:   NamespacedToScope(apiResource),
			},
			Propagation: PropagationEnabled,
			FederatedType: APIResource{
				Group:      spec.FederatedGroup,
				Version:    spec.FederatedVersion,
				Kind:       fmt.Sprintf("Federated%s", kind),
				PluralName: fmt.Sprintf("federated%s", pluralName),
				Scope:      FederatedNamespacedToScope(apiResource),
			},
		},
	}

	// Set defaults that would normally be set by the api
	SetFederatedTypeConfigDefaults(typeConfig)
	return typeConfig
}

func NamespacedToScope(apiResource metav1.APIResource) apiextv1b1.ResourceScope {
	if apiResource.Namespaced {
		return apiextv1b1.NamespaceScoped
	}
	return apiextv1b1.ClusterScoped
}

func FederatedNamespacedToScope(apiResource metav1.APIResource) apiextv1b1.ResourceScope {
	// Special-case the scope of federated namespace since it will
	// hopefully be the only instance of the scope of a federated
	// type differing from the scope of its target.
	if typeconfig.GroupQualifiedName(apiResource) == NamespaceName {
		// FederatedNamespace is namespaced to allow the control plane to run
		// with only namespace-scoped permissions e.g. to determine placement.
		return apiextv1b1.NamespaceScoped
	}
	return NamespacedToScope(apiResource)
}

func FederatedResourceFromTargetResource(typeConfig typeconfig.Interface, resource *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	fedAPIResource := typeConfig.GetFederatedType()
	targetResource := resource.DeepCopy()

	targetKind := typeConfig.GetTargetType().Kind

	// Special handling is needed for some controller set fields.
	switch targetKind {
	case NamespaceKind:
		{
			unstructured.RemoveNestedField(targetResource.Object, "spec", "finalizers")
		}
	case ServiceAccountKind:
		{
			unstructured.RemoveNestedField(targetResource.Object, SecretsField)
		}
	case ServiceKind:
		{
			var targetPorts []interface{}
			targetPorts, ok, err := unstructured.NestedSlice(targetResource.Object, "spec", "ports")
			if err != nil {
				return nil, err
			}
			if ok {
				for index := range targetPorts {
					port := targetPorts[index].(map[string]interface{})
					delete(port, "nodePort")
					targetPorts[index] = port
				}
				err := unstructured.SetNestedSlice(targetResource.Object, targetPorts, "spec", "ports")
				if err != nil {
					return nil, err
				}
			}
			unstructured.RemoveNestedField(targetResource.Object, "spec", "clusterIP")
		}
	}

	qualifiedName := NewQualifiedName(targetResource)
	resourceNamespace := getNamespace(typeConfig, qualifiedName)
	fedResource := &unstructured.Unstructured{}
	SetBasicMetaFields(fedResource, fedAPIResource, qualifiedName.Name, resourceNamespace, "")
	ownerReferences, ownerReferencesOK, errs := unstructured.NestedSlice(targetResource.Object, "metadata", "ownerReferences")
	if errs != nil {
		return nil, errs
	}
	if err := RemoveUnwantedFields(targetResource); err != nil {
		return nil, err
	}

	if ownerReferencesOK {
		err := unstructured.SetNestedSlice(targetResource.Object, ownerReferences, "metadata", "ownerReferences")
		if err != nil {
			return nil, err
		}
	}

	err := unstructured.SetNestedField(fedResource.Object, targetResource.Object, SpecField, TemplateField)
	if err != nil {
		return nil, err
	}
	err = unstructured.SetNestedStringMap(fedResource.Object, map[string]string{}, SpecField, PlacementField, ClusterSelectorField, MatchLabelsField)
	if err != nil {
		return nil, err
	}

	return fedResource, err
}

func getNamespace(typeConfig typeconfig.Interface, qualifiedName QualifiedName) string {
	if typeConfig.GetTargetType().Kind == NamespaceKind {
		return qualifiedName.Name
	}
	return qualifiedName.Namespace
}

func SetBasicMetaFields(resource *unstructured.Unstructured, apiResource metav1.APIResource, name, namespace, generateName string) {
	resource.SetKind(apiResource.Kind)
	gv := schema.GroupVersion{Group: apiResource.Group, Version: apiResource.Version}
	resource.SetAPIVersion(gv.String())
	resource.SetName(name)
	if generateName != "" {
		resource.SetGenerateName(generateName)
	}
	if apiResource.Namespaced {
		resource.SetNamespace(namespace)
	}
}

func RemoveUnwantedFields(resource *unstructured.Unstructured) error {
	unstructured.RemoveNestedField(resource.Object, "apiVersion")
	unstructured.RemoveNestedField(resource.Object, "kind")
	unstructured.RemoveNestedField(resource.Object, "status")

	// All metadata fields save labels should be cleared. Other
	// metadata fields will be set by the system on creation or
	// subsequently by controllers.
	labels, _, err := unstructured.NestedMap(resource.Object, "metadata", "labels")
	if err != nil {
		return errors.Wrap(err, "Failed to retrieve metadata.labels")
	}
	unstructured.RemoveNestedField(resource.Object, "metadata")
	if len(labels) > 0 {
		err := unstructured.SetNestedMap(resource.Object, labels, "metadata", "labels")
		if err != nil {
			return errors.Wrap(err, "Failed to set metadata.labels")
		}
	}

	return nil
}
