package constant

const (
	// role bind namespace type
	//RoleBindNamespaceTypeSystem   = "system"
	//RoleBindNamespaceTypeProject  = "project"
	//RoleBindNamespaceTypeCustomer = "customer"
	//RoleBindNamespaceTypeSpecific = "specific"
	RoleLabelPartSystem    = "system"
	RoleLabelPartCommon    = "common"
	RoleLabelPartProjectNs = "project_ns"

	//
	GroupName       = "auth.alauda.io"
	ClusterGlobal   = "global"
	ClusterBusiness = "business"

	// role bind cluster
	RoleBindClusterGlobal   = ClusterGlobal
	RoleBindClusterBusiness = ClusterBusiness
	RoleBindClusterUnlimit  = "unlimit"

	// role level
	RoleLevelPlatform  = "platform"
	RoleLevelProject   = "project"
	RoleLevelNamespace = "namespace"

	// role bind scope
	RoleBindScopeCluster   = "cluster"
	RoleBindScopeNamespace = "namespace"

	// auth.alauda.io
	RoleTemplateLabelOfficial      = "auth.alauda.io/roletemplate.official"
	RoleTemplateLabelLevel         = "auth.alauda.io/roletemplate.level"
	RoleTemplateLabelName          = "auth.alauda.io/roletemplate.name" // for labelSelector query
	RoleTemplateLabelEmail         = "auth.alauda.io/creator.email"
	UserEmailAnnotationKey         = "auth.alauda.io/user.email"
	ClusterRoleLabelRelative       = "auth.alauda.io/role.relative"
	RoleLevelLabelKey              = "auth.alauda.io/role.level"
	RoleOfficialKey                = "auth.alauda.io/role.official"
	RoleNameLabelKey               = "auth.alauda.io/role.name"
	RoleBindNamespaceTypeLabelKey  = "auth.alauda.io/role.bindnamespacetype"
	RoleBindNamespaceValueLabelKey = "auth.alauda.io/role.bindnamespacevalue"
	RoleBindScopeLabelKey          = "auth.alauda.io/role.bindscope"
	RoleBindClusterLabelKey        = "auth.alauda.io/role.bindcluster"
	UserEmailLabelKey              = UserEmailAnnotationKey

	// alauda.io
	ClusterRoleAnnotationsCurrentCluster = "alauda.io/current-cluster"
	ClusterLabelKey                      = "alauda.io/cluster"
	ProjectLabelKey                      = "alauda.io/project"
	NamespaceLabelKey                    = "alauda.io/namespace"

	// ClusterRoleNameSchema
	ClusterRoleNameSchemaSpecificBusiness = "%s-%s-b-%s"
	ClusterRoleNameSchemaCustomer         = "%s-%s"
	ClusterRoleNameSchemaProject          = "%s-pns-%s"
	ClusterRoleNameSchemaSystem           = "%s-sys-%s"
	ClusterRoleNameSchemaSystemBusiness   = "%s-sys-b-%s"
	ClusterRoleNameSchemaCluster          = "ext-%s-%s"

	// use the label to query roleTemplate quickly!
	// fr mean functionResource
	// rt mean roleTemplate
	// fr.rt mean this roleTemplate reference functionResource
	ClusterRoleLabelFunctionResourceRef         = "auth.alauda.io/rt.fr."
	ClusterRoleLabelSchemaFunctionResourceRef   = "auth.alauda.io/rt.fr.%s-%s"
	ClusterRoleLabelFunctionResourceRefSelector = "auth.alauda.io/role.level,auth.alauda.io/rt.fr.%s,auth.alauda.io/rt.fr.%s='true'"
	// query selector
	ClusterRoleLabelRelativeSelector = "auth.alauda.io/role.relative,auth.alauda.io/role.relative=%s"
)
