package namespaces

import (
	"context"
	"strings"

	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Namespace is a wrapper to act as a Defaulter()
type Namespace struct {
	v1.Namespace
}

// ValidateDeleteWithClient validate if the namespace can be deleted
func (ns *Namespace) ValidateDeleteWithClient(req admission.Request, client client.Client) error {
	nsName := req.Name
	namespace := &v1.Namespace{}
	err := client.Get(context.TODO(), types.NamespacedName{Name: nsName}, namespace)
	if err != nil {
		return err
	}
	ns.Namespace = *namespace

	klog.Infof("hi validating namespace: %+v", ns)

	annotations := ns.Annotations
	// check if has force delete annotation
	if len(annotations) > 0 {
		// has force delete annotation will allow delete
		if forceDelete, hasForceKey := annotations[constant.AnnotationForceDelete]; hasForceKey && strings.ToLower(forceDelete) == "true" {
			return nil
		}
	}

	labels := ns.Labels
	// no labels, allow delete
	if len(labels) == 0 {
		return nil
	} else {
		// has project label and projectName equals ns name, deny delete
		if projectName, hasProjectKey := labels[constant.LabelProject]; hasProjectKey && projectName == nsName {
			return errors.NewBadRequest("default namespace under project can not be delete, except that force delete annotation is enabled")
		}
	}
	return nil
}

// ValidateCreateWithClient
func (ns *Namespace) ValidateCreateWithClient(client client.Client) error {
	return nil
}

// ValidateUpdateWithClient
func (ns *Namespace) ValidateUpdateWithClient(old runtime.Object, client client.Client) error {
	return nil
}

// ValidateCreate
func (ns *Namespace) ValidateCreate() error {
	return nil
}

// ValidateUpdate
func (ns *Namespace) ValidateUpdate(old runtime.Object) error {
	return nil
}

// ValidateDelete
func (ns *Namespace) ValidateDelete() error {
	return nil
}

func (ns *Namespace) DeepCopyObject() runtime.Object {
	o := ns.Namespace.DeepCopyObject().(*v1.Namespace)
	return &Namespace{
		*o,
	}
}
