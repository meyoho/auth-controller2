package namespaces

import (
	"net/http"

	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/base"
)

//GetHandler get a handler by name
func GetHandler(name string) http.Handler {
	handler := base.ValidatingWebhookFor(&Namespace{})
	base.InjectLogger(handler, name)
	return handler
}
