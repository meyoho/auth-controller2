package webhooks

import (
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/common"
	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/namespaces"
	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/projects"
	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/userbindings"
)

// Validating path
var (
	//DefaultNamespace prevent deletion of namespace with same name with project, in which namespace blongs to
	DefaultNamespace = "default-namespace"

	// ProjectClusters validate that project has at least one clusters
	ProjectClusters = "project-clusters"

	// ProjectStatus set project status to Creating when init
	ProjectStatus = "project-status"

	UserBinding = "userbinding"
)

// Mutating path
var (
	// Creator validate that project has at least one clusters
	Creator = "creator"

	// UpdatedAt validate that project has at least one clusters
	UpdatedAt = "updated-at"
)

// getPath generate path for a handler. The leading / is mandatory...
func getPath(name string) string {
	return "/" + name
}

//RegisterWebhooks register all the webhook handlers
func RegisterWebhooks(ws *webhook.Server) {
	ws.Register(getPath(DefaultNamespace), namespaces.GetHandler(DefaultNamespace))
	ws.Register(getPath(ProjectClusters), projects.GetHandler(ProjectClusters))
	ws.Register(getPath(ProjectStatus), projects.GetMutatingHandler(ProjectStatus))
	ws.Register(getPath(UserBinding), userbindings.GetHandler(UserBinding))

	ws.Register(getPath(Creator), common.GetHandler(Creator))
	ws.Register(getPath(UpdatedAt), common.GetHandler(UpdatedAt))
}
