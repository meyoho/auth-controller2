package projects

import (
	"net/http"

	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/base"
)

//GetHandler get a handler by name
func GetHandler(name string) http.Handler {
	handler := base.ValidatingWebhookFor(&Project{})
	base.InjectLogger(handler, name)
	return handler
}

//GetMutatingHandler get a handler by name
func GetMutatingHandler(name string) http.Handler {
	handler := base.MutatingWebhookFor(&Project{})
	base.InjectLogger(handler, name)
	return handler
}
