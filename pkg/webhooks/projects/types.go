package projects

import (
	// "context"
	"context"
	"fmt"
	"net/http"

	// cauth "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/utils"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Project is a wrapper to act as a Defaulter()
type Project struct {
	authv1.Project
}

// ValidateUpdate validate if the project can be updated
func (project *Project) ValidateUpdate(old runtime.Object) error {
	klog.Infof("hi validating project: %+v", project)
	oldProject := old.(*Project)

	// the project before update has no clusters will be ignored
	if len(oldProject.Project.Spec.Clusters) == 0 {
		return nil
	}

	return nil
}

// ValidateCreate
func (project *Project) ValidateCreate() error {
	return nil
}

// ValidateDelete
func (project *Project) ValidateDelete() error {
	return nil
}

func isProjectDeleted(project *Project) bool {
	ac, _ := meta.Accessor(project)
	deleteTime := ac.GetDeletionTimestamp()
	if deleteTime != nil {
		return true
	}

	return false
}

func hasClusterQuotaChanged(oldCluster *authv1.ProjectClusters, newCluster *authv1.ProjectClusters) bool {
	// new added cluster, always changed
	if oldCluster == nil {
		return true
	}

	// new resource and quantity exist in old cluster
	for resourceName, quantity := range newCluster.Quota {
		oldQuantity, exist := oldCluster.Quota[resourceName]
		if !exist || oldQuantity != quantity {
			return true
		}
	}

	// old resource and quantity remain in new cluster
	for resourceName, quantity := range oldCluster.Quota {
		newQuantity, exist := newCluster.Quota[resourceName]
		if !exist || newQuantity != quantity {
			return true
		}
	}

	return false
}

// ValidateUpdateWithClient validate if the project can be updated
func (project *Project) ValidateUpdateWithClient(old runtime.Object, cli client.Client) error {
	klog.Infof("hi validating update project with client: %+v", project)

	// hijack deleted project
	if isProjectDeleted(project) {
		return nil
	}

	oldProject, ok := old.(*Project)
	if !ok {
		return kerrors.NewBadRequest("Failed to decode the oldObject")
	}

	// record the exist clusters,
	// when validate update projectquota should be greater than used,
	// the new added cluster will not check
	existClusters := make(map[string]*authv1.ProjectClusters)
	for _, cluster := range oldProject.Project.Spec.Clusters {
		existClusters[cluster.Name] = cluster
	}

	for _, cluster := range project.Project.Spec.Clusters {
		klog.Infof("validating update project with client, validating cluster: %+v", cluster)

		// if quota unchanged, then will igonre check of cluster and projectquota crd,
		// to fix AIT-910
		if !hasClusterQuotaChanged(existClusters[cluster.Name], cluster) {
			continue
		}

		cc, err := utils.GetClusterClient(cluster.Name, cli)
		if err != nil {
			return err
		}

		quotas := &authv1.ProjectQuotaList{}
		err = cc.List(context.TODO(), quotas, &client.ListOptions{
			Limit: 1,
		})
		if err != nil {
			return err
		}

		_, exist := existClusters[cluster.Name]
		if !exist {
			// new added cluster will not check the projectquota is greater than used
			// but check cluster and projectquota crd existence
			continue
		}

		quota := &authv1.ProjectQuota{}
		err = cc.Get(context.TODO(), types.NamespacedName{Name: project.Name}, quota)
		klog.Infof("validating update project with client, exist projectquota: %+v", quota)
		if err != nil && !kerrors.IsNotFound(err) {
			return err
		}

		for resourceName, quantity := range cluster.Quota {
			usedQuantity, limited := quota.Status.Hard[resourceName]
			if !limited {
				continue
			}
			if (&quantity).Cmp(usedQuantity) < 0 {
				return &kerrors.StatusError{
					ErrStatus: metav1.Status{
						Status: metav1.StatusFailure,
						Code:   http.StatusUnprocessableEntity,
						Reason: metav1.StatusReasonInvalid,
						Message: fmt.Sprintf("ProjectQuota(%s) under cluster(%s) resource(%s) should be large than used(%v), request(%v)",
							project.Name, cluster.Name, resourceName, (&usedQuantity).String(), (&quantity).String()),
					}}
			}
		}
	}
	return nil
}

// ValidateCreateWithClient
func (project *Project) ValidateCreateWithClient(cli client.Client) error {
	klog.Infof("hi validating create project with client: %+v", project)
	for _, cluster := range project.Project.Spec.Clusters {
		klog.Infof("validating create project with client, validating cluster: %+v", cluster)
		cc, err := utils.GetClusterClient(cluster.Name, cli)
		if err != nil {
			return err
		}

		// used to check if projectquote crd exist
		quotas := &authv1.ProjectQuotaList{}
		err = cc.List(context.TODO(), quotas, &client.ListOptions{
			Limit: 1,
		})
		if err != nil {
			return err
		}

	}
	return nil
}

// ValidateDeleteWithClient
func (project *Project) ValidateDeleteWithClient(req admission.Request, client client.Client) error {
	return nil
}

func (project *Project) DeepCopyObject() runtime.Object {
	o := project.Project.DeepCopyObject().(*authv1.Project)
	return &Project{
		*o,
	}
}

func (project *Project) MutateCreate(req admission.Request) error {
	return nil
}

func (project *Project) MutateUpdate(req admission.Request) error {
	return nil
}

func (project *Project) MutateCreateWithClient(req admission.Request, client client.Client) error {
	return nil
}

func (project *Project) MutateUpdateWithClient(req admission.Request, client client.Client) error {
	return nil
}
