package common

import (
	"strings"
	"time"

	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Workload is a wrapper to act as a Mutator
type Workload struct {
	unstructured.Unstructured
}

// Default do nothing... The part that matters is in Decode
func (w *Workload) MutateCreate(req admission.Request) error {
	if strings.Contains(req.UserInfo.Username, ":") {
		klog.Info("user info in admission request contains invalid label char, may be a system user, ignore it:", req.UserInfo.Username)
		return nil
	}

	ac, err := meta.Accessor(w)
	if err != nil {
		klog.Error("get meta accessor error for object:", err)
		return err
	}

	ano := ac.GetAnnotations()
	if ano == nil {
		ano = make(map[string]string)
	}
	ano[constant.AnnotationCreator] = req.UserInfo.Username
	ac.SetAnnotations(ano)

	return nil
}

func (w *Workload) MutateUpdate(req admission.Request) error {
	ac, err := meta.Accessor(w)
	if err != nil {
		klog.Error("get meta accessor error for object:", err)
		return err
	}

	ano := ac.GetAnnotations()
	if ano == nil {
		ano = make(map[string]string)
	}
	ano[constant.AnnotationUpdatedAt] = time.Now().Format(time.RFC3339)
	ac.SetAnnotations(ano)

	return nil
}

//DeepCopyObject ....
func (w *Workload) DeepCopyObject() runtime.Object {
	o := w.Unstructured.DeepCopyObject().(*unstructured.Unstructured)
	return &Workload{
		*o,
	}
}
