package userbindings

import (
	"context"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/mathildetech/auth-controller2/pkg/utils"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"bitbucket.org/mathildetech/auth-controller2/pkg/webhooks/base"
	"k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

//GetHandler get a handler by name
func GetHandler(name string) http.Handler {
	log.Printf("initGetHandler : %+v", name)
	handler := ValidatingWebhookFor()
	base.InjectLogger(handler, name)
	return handler
}

// ValidatingWebhookFor creates a new Webhook for validating the provided type.
func ValidatingWebhookFor() *admission.Webhook {
	return &admission.Webhook{
		Handler: &validatingHandler{},
	}
}

type validatingHandler struct {
	decoder *admission.Decoder
	client  client.Client
}

var (
	_                   admission.DecoderInjector = &validatingHandler{}
	userBindingResource                           = schema.GroupResource{
		Group:    rc.GroupName,
		Resource: "userbindings",
	}
)

// InjectDecoder injects the decoder into a validatingHandler.
func (h *validatingHandler) InjectDecoder(d *admission.Decoder) error {
	h.decoder = d
	return nil
}

// InjectClient injects the client.Client into a validatingHandler.
func (h *validatingHandler) InjectClient(c client.Client) error {
	h.client = c
	return nil
}

// Handle handles admission requests.
func (h *validatingHandler) Handle(ctx context.Context, req admission.Request) admission.Response {
	if strings.Contains(req.UserInfo.Username, ":") {
		klog.Info("user info in admission request contains invalid label char, may be a system user, ignore it:", req.UserInfo.Username)
		return admission.Allowed("")
	}

	accessReview := utils.SimpleAccessReview{
		Client: h.client,
	}

	// Get the object in the request
	obj := &authv1.UserBinding{}
	if req.Operation == v1beta1.Create {
		err := h.decoder.Decode(req, obj)
		if err != nil {
			return base.Errored(http.StatusBadRequest, err)
		}

		log.Printf("Handle Create: %+v", obj)
		constraints := utils.Newconstraints(obj)
		verify, err := accessReview.Verify(req.UserInfo.Username, "create", userBindingResource, constraints)
		if err != nil {
			return base.Denied(err.Error())
		}
		if !verify {
			return base.Denied(utils.ErrNotEnoughPerms)
		}
	}

	if req.Operation == v1beta1.Update {
		oldObj := &authv1.UserBinding{}

		err := h.decoder.DecodeRaw(req.Object, obj)
		if err != nil {
			return base.Errored(http.StatusBadRequest, err)
		}
		err = h.decoder.DecodeRaw(req.OldObject, oldObj)
		if err != nil {
			return base.Errored(http.StatusBadRequest, err)
		}

		log.Printf("Handle Update obj: %+v, oldObj: %+v", obj, oldObj)
		constraints := utils.Newconstraints(obj)
		verify, err := accessReview.Verify(req.UserInfo.Username, "update", userBindingResource, constraints)
		if err != nil {
			return base.Denied(err.Error())
		}
		if !verify {
			return base.Denied(utils.ErrNotEnoughPerms)
		}
	}

	if req.Operation == v1beta1.Delete {
		obj := &authv1.UserBinding{}
		err := h.client.Get(context.TODO(), types.NamespacedName{Name: req.Name}, obj)
		log.Printf("Handle Delete obj: %+v", obj)
		if err != nil {
			return base.Errored(http.StatusBadRequest, err)
		}
		constraints := utils.Newconstraints(obj)
		verify, err := accessReview.Verify(req.UserInfo.Username, "delete", userBindingResource, constraints)
		if err != nil {
			return base.Denied(err.Error())
		}
		if !verify {
			return base.Denied(utils.ErrNotEnoughPerms)
		}
	}

	return admission.Allowed("")
}
