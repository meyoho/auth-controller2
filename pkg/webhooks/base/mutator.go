package base

import (
	"context"
	"encoding/json"
	"net/http"

	"k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Validator defines functions for validating an operation
type Mutator interface {
	runtime.Object
	MutateCreate(req admission.Request) error
	MutateUpdate(req admission.Request) error
}

type MutatorWithClient interface {
	runtime.Object
	MutateCreateWithClient(req admission.Request, client client.Client) error
	MutateUpdateWithClient(req admission.Request, client client.Client) error
}

// MutatingWebhookFor creates a new Webhook for mutate the provided type.
func MutatingWebhookFor(mutator Mutator) *admission.Webhook {
	return &admission.Webhook{
		Handler: &mutatingHandler{mutator: mutator},
	}
}

type mutatingHandler struct {
	mutator Mutator
	decoder *admission.Decoder
	client  client.Client
}

var _ admission.DecoderInjector = &mutatingHandler{}

// InjectDecoder injects the decoder into a mutatingHandler.
func (h *mutatingHandler) InjectDecoder(d *admission.Decoder) error {
	h.decoder = d
	return nil
}

// InjectClient injects the client.Client into a mutatingHandler.
func (h *mutatingHandler) InjectClient(c client.Client) error {
	h.client = c
	return nil
}

// Handle handles admission requests.
func (h *mutatingHandler) Handle(ctx context.Context, req admission.Request) admission.Response {
	if h.mutator == nil {
		panic("mutator should never be nil")
	}

	obj := h.mutator.DeepCopyObject().(Mutator)
	err := h.decoder.Decode(req, obj)
	if err != nil {
		klog.Error("decode admission request error:", err)
		return Errored(http.StatusBadRequest, err)
	}

	if req.Operation == v1beta1.Create {
		err = obj.MutateCreate(req)
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}

		mutatorClient, isWithClient := h.mutator.DeepCopyObject().(MutatorWithClient)
		if isWithClient {
			err = h.decoder.Decode(req, mutatorClient)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
			err = mutatorClient.MutateCreateWithClient(req, h.client)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
		}
	} else if req.Operation == v1beta1.Update {
		err = obj.MutateUpdate(req)
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}

		mutatorClient, isWithClient := h.mutator.DeepCopyObject().(MutatorWithClient)
		if isWithClient {
			err = h.decoder.Decode(req, mutatorClient)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
			err = mutatorClient.MutateUpdateWithClient(req, h.client)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
		}
	}

	marshalled, err := json.Marshal(obj)
	if err != nil {
		klog.Error("marshal mutated object error:", err)
		return Errored(http.StatusInternalServerError, err)
	}

	// Create the patch
	return admission.PatchResponseFromRaw(req.Object.Raw, marshalled)
}
