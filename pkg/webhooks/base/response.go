package base

import (
	"net/http"

	admissionv1beta1 "k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Errored creates a new Response for error-handling a request.
func Errored(code int32, err error) admission.Response {
	statusErr, ok := err.(*errors.StatusError)
	if !ok {
		statusErr = errors.NewInternalError(err)
		statusErr.ErrStatus.Code = code
		statusErr.ErrStatus.Reason = metav1.StatusReason(http.StatusText(int(code)))
		statusErr.ErrStatus.Message = err.Error()
	}
	return admission.Response{
		AdmissionResponse: admissionv1beta1.AdmissionResponse{
			Allowed: false,
			Result:  &statusErr.ErrStatus,
		},
	}
}

func Denied(message string) admission.Response {
	code := http.StatusForbidden
	resp := admission.Response{
		AdmissionResponse: admissionv1beta1.AdmissionResponse{
			Allowed: false,
			Result: &metav1.Status{
				Code:    int32(code),
				Reason:  metav1.StatusReasonForbidden,
				Message: message,
			},
		},
	}
	return resp
}
