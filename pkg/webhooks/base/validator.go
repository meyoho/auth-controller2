package base

import (
	"context"
	"net/http"

	"k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Validator defines functions for validating an operation
type Validator interface {
	admission.Validator
}

type ValidatorWithClient interface {
	runtime.Object
	ValidateCreateWithClient(client client.Client) error
	ValidateUpdateWithClient(old runtime.Object, client client.Client) error
	ValidateDeleteWithClient(req admission.Request, client client.Client) error
}

// ValidatingWebhookFor creates a new Webhook for validating the provided type.
func ValidatingWebhookFor(validator Validator) *admission.Webhook {
	return &admission.Webhook{
		Handler: &validatingHandler{validator: validator},
	}
}

type validatingHandler struct {
	validator Validator
	decoder   *admission.Decoder
	client    client.Client
}

var _ admission.DecoderInjector = &validatingHandler{}

// InjectDecoder injects the decoder into a validatingHandler.
func (h *validatingHandler) InjectDecoder(d *admission.Decoder) error {
	h.decoder = d
	return nil
}

// InjectClient injects the client.Client into a validatingHandler.
func (h *validatingHandler) InjectClient(c client.Client) error {
	h.client = c
	return nil
}

// Handle handles admission requests.
func (h *validatingHandler) Handle(ctx context.Context, req admission.Request) admission.Response {
	if h.validator == nil {
		panic("validator should never be nil")
	}

	// Get the object in the request
	obj := h.validator.DeepCopyObject().(Validator)
	if req.Operation == v1beta1.Create {
		err := h.decoder.Decode(req, obj)
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}

		err = obj.ValidateCreate()
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}

		validatorClient, isWithClient := h.validator.DeepCopyObject().(ValidatorWithClient)
		if isWithClient {
			err = h.decoder.Decode(req, validatorClient)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
			err = validatorClient.ValidateCreateWithClient(h.client)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
		}
	}

	if req.Operation == v1beta1.Update {
		oldObj := obj.DeepCopyObject()

		err := h.decoder.DecodeRaw(req.Object, obj)
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}
		err = h.decoder.DecodeRaw(req.OldObject, oldObj)
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}

		err = obj.ValidateUpdate(oldObj)
		if err != nil {
			return Errored(http.StatusBadRequest, err)
		}

		validatorClient, isWithClient := h.validator.DeepCopyObject().(ValidatorWithClient)
		klog.Infof("hi validating update with client: %v", isWithClient)
		if isWithClient {
			err = h.decoder.Decode(req, validatorClient)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
			err = validatorClient.ValidateUpdateWithClient(oldObj, h.client)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
		}
	}

	if req.Operation == v1beta1.Delete {
		validatorClient, isWithClient := h.validator.DeepCopyObject().(ValidatorWithClient)
		if isWithClient {
			err := h.decoder.DecodeRaw(req.OldObject, validatorClient)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
			err = validatorClient.ValidateDeleteWithClient(req, h.client)
			klog.Infof("hi validating result: %v", err)
			if err != nil {
				return Errored(http.StatusBadRequest, err)
			}
		}
	}

	return admission.Allowed("")
}
