package base

import (
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
)

//InjectLogger inject logger to a named handler, ignores the errors
func InjectLogger(logger inject.Logger, name string) {
	l := log.Log.WithName(name)
	err := logger.InjectLogger(l)
	if err != nil {
		klog.Warning("inject logger to webhook handler error: ", err)
	}
}
