/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package roletemplate

import (
	wg "alauda.io/warpgate"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	"context"
	"fmt"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("roletemplate-controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new RoleTemplate Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.RoleTemplate) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.RoleTemplate))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileRoleTemplate{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("roletemplate-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to RoleTemplate
	err = c.Watch(&source.Kind{Type: &authv1beta1.RoleTemplate{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileRoleTemplate{}

// ReconcileRoleTemplate reconciles a RoleTemplate object
type ReconcileRoleTemplate struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a RoleTemplate object and makes changes based on the state read
// and what is in the RoleTemplate.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=auth.alauda.io,resources=roletemplates,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=roletemplates/status,verbs=get;update;patch
func (r *ReconcileRoleTemplate) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log.Info("roletemplate-controller start Reconcile", "roleTemplate:", request.Name)
	// Fetch the RoleTemplate instance
	instance := &authv1beta1.RoleTemplate{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}
	if err := instance.Validate(); err != nil {
		return reconcile.Result{}, nil
	}
	if instance.IsOfficial() {
		return reconcile.Result{}, nil
	}
	//if instance.IsNeedUpdateLabel() {
	//	log.Info("need update roleTemplateLabel", "RoleTemplate Name:", instance.Name)
	//	instance.MaintainFunctionResourceRefLabelConsistent()
	//	err = r.Update(context.TODO(), instance.DeepCopy())
	//	if err != nil {
	//		log.Error(err, "update RoleTemplateLabel Error", "RoleTemplate Name:", instance.Name)
	//	} else {
	//		log.Info("update RoleTemplateLabel Success", "RoleTemplate Name:", instance.Name)
	//	}
	//	return reconcile.Result{}, nil
	//}
	log.Info("Create or Update ClusterRoles", "RoleTemplate Name:", instance.Name)
	UpdateClusterRoles(instance, r.Client)
	return reconcile.Result{}, nil
}

func UpdateClusterRoles(r *authv1beta1.RoleTemplate, cli client.Client) {
	roleMap := r.ConvertToClusterRoles(cli)
	//clusterRoleList, err := r.GetReferenceClusterRoleList(cli)
	//if err != nil {
	//	log.Error(err, "GetReferenceClusterRoleList Error", "roleTemplate:", r.Name)
	//}
	//if clusterRoleList != nil {
	//	for _, clusterRole := range clusterRoleList.Items {
	//		if _, ok := roleMap[clusterRole.Name]; !ok {
	//			if _, ok := clusterRole.Annotations[rc.ClusterRoleAnnotationsCurrentCluster]; !ok {
	//				log.Info("Delete ReferenceClusterRole Jump", "roleTemplate:", r.Name, "roleName:", clusterRole.Name)
	//				continue
	//			}
	//			// cancel delete notPlatformGenerate ClusterRole
	//			if !apis.IsPlatformGenerateRole(&clusterRole) {
	//				log.Info("Cancel Delete Invalid ClusterRole", "roleTemplate:", r.Name, "roleName:", clusterRole.Name)
	//				continue
	//			}
	//			log.Info("delete clusterRole:", "aa", clusterRole.Name)
	//			err = cli.Delete(context.TODO(), clusterRole.DeepCopy())
	//			if err != nil {
	//				if !errors.IsNotFound(err) {
	//					log.Error(err, "Delete ReferenceClusterRole Error", "roleTemplate:", r.Name, "roleName:", clusterRole.Name)
	//				}
	//			} else {
	//				log.Info("Delete ClusterRole Success", "roleTemplate:", r.Name, "roleName:", clusterRole.Name)
	//			}
	//		}
	//	}
	//}
	for _, clusterRole := range roleMap {
		r.AddExtraDataForClusterRole(&clusterRole)
		//cancel crate notPlatformGenerate ClusterRole
		if !apis.IsGlobalClusterRoleAndPlatformGenerateRole(&clusterRole) {
			log.Info("Cancel Create ClusterRole Because this Role is Invalid", "RoleName:", clusterRole.Name, "RoleTemplateName:", r.Name)
			continue
		}
		err := cli.Create(context.TODO(), clusterRole.DeepCopy())
		if err != nil {
			if errors.IsAlreadyExists(err) {
				err = cli.Update(context.TODO(), clusterRole.DeepCopy())
				if err != nil {
					log.Error(err, "Update ClusterRole Error", "roleTemplate:", r.Name, "roleName:", clusterRole.Name)
				}
			} else {
				log.Error(err, "Create ClusterRole Error", "roleTemplate:", r.Name, "roleName:", clusterRole.Name)
			}
		} else {
			log.Info("Create ClusterRole Success", "RoleName:", clusterRole.Name, "RoleTemplateName:", r.Name)
		}
	}
}

func IsFeatureGateEnabld(featureGate string) (bool, error) {
	const (
		tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	)
	token, err := ioutil.ReadFile(tokenFile)
	if err != nil {
		return false, err
	}
	wwg := wg.NewWarpGate(wg.Config{
		AuthorizationToken: string(token),
		APIEndpoint:        "http://archon"})
	enabled, err := wwg.IsFeatureGateEnabled(featureGate)
	if err != nil {
		return false, err
	}
	return enabled, nil
}
