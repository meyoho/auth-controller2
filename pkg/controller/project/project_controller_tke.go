/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package project

import (
	"errors"
	"fmt"
	"reflect"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	businessv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/business/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"golang.org/x/net/context"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// syncTkeProject ensure tke project is created before creating alauda project associate objects
func (r *ReconcileProject) syncTkeProject(authP *authv1.Project) error {
	if !needSyncTke() {
		return nil
	}
	log.Info("Start sync alauda project to tke project", "name", authP.Name)
	projects := &businessv1.ProjectList{}
	labelSelector, err := labels.Parse(fmt.Sprintf("%s=%s", constant.LabelProject, authP.Name))
	if err != nil {
		return err
	}
	err = r.List(context.TODO(), projects, &client.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		log.Error(err, "Failed to query projects from tke", "name", authP.Name)
		return err
	}
	switch len(projects.Items) {
	case 0:
		return r.creatTkeProject(authP)
	case 1:
		return r.updateTkeProject((projects.Items[0]), authP)
	default:
		return errors.New("Multiple tke Projects has been associated with one alauda project")
	}
}

func needSyncTke() bool {
	return apis.NeedSyncTkeProject()
}

func convertQuota(quotas []*authv1.ProjectClusters) businessv1.ClusterHard {
	clusters := businessv1.ClusterHard{}
	for _, quota := range quotas {
		clusters[quota.Name] = businessv1.HardQuantity{}
		if len(quota.Quota) > 0 {
			resources := businessv1.ResourceList{}
			for res, quantity := range quota.Quota {
				resources[string(res)] = quantity
			}
			clusters[quota.Name] = businessv1.HardQuantity{
				Hard: resources,
			}
		}
	}

	return clusters

}

// newTkeProject creates a new Project in group [businessv1.GroupName].
// It also sets the label for indicate synced from alauda project
func newTkeProject(authP authv1.Project) *businessv1.Project {
	labels := map[string]string{}
	if len(authP.Labels) != 0 {
		labels = authP.Labels
	}

	// ensure label exist
	labels[constant.LabelProject] = authP.Name

	annotations := map[string]string{}
	if len(authP.Annotations) != 0 {
		annotations = authP.Annotations
	}

	return &businessv1.Project{
		ObjectMeta: metav1.ObjectMeta{
			Name:        authP.Name,
			Labels:      labels,
			Annotations: annotations,
		},
		Spec: businessv1.ProjectSpec{
			DisplayName:       authP.Name,
			ParentProjectName: labels[constant.LabelProjectParent],
			Clusters:          convertQuota(authP.Spec.Clusters),
		},
	}
}

// createTkeProject ensure tke project is created before creating alauda project associate objects
func (r *ReconcileProject) creatTkeProject(authP *authv1.Project) error {
	if !needSyncTke() {
		return nil
	}
	log.Info("Alauda has project, but tke not, will create", "name", authP.Name)
	project := newTkeProject(*authP)
	err := r.Create(context.TODO(), project)

	log.Info("Create tke project", "name", authP.Name, "error", err)

	return err
}

// updateTkeProject ensure tke project is alway equals to alauda project in cluster quotas
// also, ensure that, labels and annotations in alauda project also contained in tke project
func (r *ReconcileProject) updateTkeProject(projectOld businessv1.Project, authP *authv1.Project) error {
	if !needSyncTke() {
		return nil
	}
	log.Info("Update tke project", "name", authP.Name)

	projectNew := newTkeProject(*authP)
	projectOld = setDefaults(projectOld)

	var labelChanged, annotationChanged, quotaChanged bool
	for key, value := range projectNew.Labels {
		if valueOld, ok := projectOld.Labels[key]; !ok || valueOld != value {
			labelChanged = true
			projectOld.Labels[key] = value
		}
	}
	for key, value := range projectNew.Annotations {
		if valueOld, ok := projectOld.Annotations[key]; !ok || valueOld != value {
			annotationChanged = true
			projectOld.Annotations[key] = value
		}
	}
	if !reflect.DeepEqual(projectOld.Spec.Clusters, projectNew.Spec.Clusters) {
		quotaChanged = true
		projectOld.Spec.Clusters = projectNew.Spec.Clusters
	}

	if labelChanged || annotationChanged || quotaChanged {
		err := r.Update(context.TODO(), &projectOld)
		if err != nil {
			log.Error(err, "Failed to update project on tke", "name", authP.Name, "error", err)
			return err
		}
	}
	log.Info("Successfully update tke project or no need to update", "name", authP.Name)
	return nil
}

// deleteTkeProject ensure tke project with specified label are deleted
func (r *ReconcileProject) deleteTkeProject(authP *authv1.Project) error {
	if !needSyncTke() {
		return nil
	}

	log.Info("Delete tke project", "name", authP.Name)

	labelSelector, err := labels.Parse(fmt.Sprintf("%s=%s", constant.LabelProject, authP.Name))
	if err != nil {
		return err
	}
	projects := &businessv1.ProjectList{}
	err = r.List(context.TODO(), projects, &client.ListOptions{
		LabelSelector: labelSelector,
	})

	if err != nil {
		log.Error(err, "Failed to query projects from tke", "name", authP.Name)
		return err
	}

	for _, project := range projects.Items {
		err = r.Delete(context.TODO(), &project)
		if err != nil && !kerrors.IsNotFound(err) {
			return err
		}
	}

	return nil

}

func setDefaults(project businessv1.Project) businessv1.Project {
	if len(project.Labels) == 0 {
		project.Labels = make(map[string]string)
	}
	if len(project.Annotations) == 0 {
		project.Annotations = make(map[string]string)
	}
	return project
}
