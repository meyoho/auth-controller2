/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package project

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/kubefed"

	"strings"

	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/workqueue"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("project-controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Project Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this auth.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	if !(apis.IsResourceEnabled(apis.Project) && apis.IsResourceEnabled(apis.ProjectBinding)) {
		log.Info(fmt.Sprintf("%s controller not enabled! project enable status %v, projectbinding enable status %v",
			apis.Project, apis.IsResourceEnabled(apis.Project), apis.IsResourceEnabled(apis.ProjectBinding)))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileProject{Client: mgr.GetClient(), scheme: mgr.GetScheme(), Config: mgr.GetConfig()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("project-controller", mgr, controller.Options{Reconciler: r, MaxConcurrentReconciles: apis.ProjectWorkers})
	if err != nil {
		return err
	}

	// Watch for changes to Project
	err = c.Watch(&source.Kind{Type: &authv1.Project{}}, &handler.Funcs{
		CreateFunc: func(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "Project CreateEvent received with no metadata", "event", evt)
				return
			}
			log.Info("Project CreateEvent received with", "name", evt.Meta.GetName(), "queue size", q.Len())
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
		UpdateFunc: func(evt event.UpdateEvent, q workqueue.RateLimitingInterface) {
			if evt.MetaNew == nil {
				log.Error(nil, "Project UpdateEvent received with no metadata", "event", evt)
				return
			}
			log.Info("Project UpdateEvent received with", "name", evt.MetaNew.GetName(), "queue size", q.Len())
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.MetaNew.GetName(),
				Namespace: evt.MetaNew.GetNamespace(),
			}})
		},
		DeleteFunc: func(evt event.DeleteEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "Project DeleteEvent received with no metadata", "event", evt)
				return
			}
			log.Info("Project DeleteEvent received with", "name", evt.Meta.GetName(), "queue size", q.Len())
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
	})

	// watch ns created by Project
	err = c.Watch(&source.Kind{Type: &corev1.Namespace{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &authv1.Project{},
	})
	if err != nil {
		return err
	}

	// watch ProjectBinding created by Project
	err = c.Watch(&source.Kind{Type: &authv1.ProjectBinding{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &authv1.Project{},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileProject{}

// ReconcileProject reconciles a Project object
type ReconcileProject struct {
	client.Client
	scheme *runtime.Scheme
	*rest.Config
}

func (r *ReconcileProject) HasCrdExist(obj runtime.Object) bool {
	err := r.Get(context.TODO(), types.NamespacedName{Name: "test"}, obj)
	if err != nil && strings.Contains(err.Error(), "no matches for kind") {
		return false
	}
	return true
}

// Reconcile reads that state of the cluster for a Project object and makes changes based on the state read
// and what is in the Project.Spec
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=auth.alauda.io,resources=projects,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileProject) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log.Info("Reconcile in Project controller", "name", request.Name)

	bindingExist := r.HasCrdExist(&authv1.ProjectBinding{})
	if !bindingExist {
		log.Info("ProjectBinding is not deployed on this cluster, Project instance marked as processed")
		return reconcile.Result{}, nil
	}

	err := r.syncHandler(request)
	log.Info("Reconcile in Project controller", "name", request.Name, "error", err)
	r.handleStatus(request, err)

	return reconcile.Result{}, err
}

// handleStatus
func (r *ReconcileProject) handleStatus(request reconcile.Request, err error) {
	//get the project object
	project := &authv1.Project{}

	getProjectErr := r.Get(context.TODO(), types.NamespacedName{Name: request.Name}, project)

	if getProjectErr == nil && err != nil {
		project.Status.Phase = auth.ProjectStatusError
		project.Status.Reason = string(err.Error())
		r.updateProjectStatus(project)
	}
	specContent, _ := json.Marshal(project.Spec)
	annotationContent, _ := json.Marshal(project.Annotations)
	labelContent, _ := json.Marshal(project.Labels)
	specAnnotation := append(specContent, annotationContent...)
	allContent := append(specAnnotation, labelContent...)

	if project.Status.ResourceVersion != util.GetMD5Hash(string(allContent[:])) {
		project.Status.ResourceVersion = util.GetMD5Hash(string(allContent[:]))
		r.updateProjectStatus(project)
	}
}

// syncHandler is the main logic body
func (r *ReconcileProject) syncHandler(request reconcile.Request) (err error) {
	log.Info("syncing project", "name", request.Name)

	//get the project object
	project := &authv1.Project{}

	getProjectErr := r.Get(context.TODO(), types.NamespacedName{Name: request.Name}, project)

	if getProjectErr != nil {
		if errors.IsNotFound(getProjectErr) {
			// Handle deleted event
			log.Info("project instance deleted, will delete related ProjectQuota/ProjectBinding")

			// update to terminating status
			project.Status.Phase = auth.ProjectStatusTerminating
			r.updateProjectStatus(project)

			err = r.setNamespaceForceDeleteAnnotation(request.Name)
			if err != nil {
				return
			}
			err = r.deleteTkeProject(project)
			if err != nil {
				return
			}
			return r.deleteProjectClusters(request.Name)
		}

		// always return nil, when error occurs
		return nil
	}

	log.Info("Successfully get the project instance", "name", request.Name)

	// ensure the creating status
	if project.Status.Phase == "" {
		project.Status.Phase = auth.ProjectStatusCreating
		r.updateProjectStatus(project)
	}

	project.SetDefaults()

	finalizer := apis.FlagTkeSwitch

	if project.DeletionTimestamp != nil {
		log.Info("The project has DeletionTimestamp", "name", request.Name)
		// update to terminating status
		project.Status.Phase = auth.ProjectStatusTerminating
		r.updateProjectStatus(project)

		// contains tke finalizer
		if contains(project.Finalizers, finalizer) {
			log.Info("The project has tke finalizer", "name", request.Name)
			err = r.deleteTkeProject(project)
			log.Info("Delete tke project", "name", request.Name, "error", err)
			if err != nil {
				return err
			}
			log.Info("Remove the tke finalizer from project", "name", request.Name)
			project.Finalizers = removeItem(project.Finalizers, finalizer)
			return r.updateProject(project)
		}

		return nil
	}
	specContent, _ := json.Marshal(project.Spec)
	annotationContent, _ := json.Marshal(project.Annotations)
	labelContent, _ := json.Marshal(project.Labels)
	specAnnotation := append(specContent, annotationContent...)
	allContent := append(specAnnotation, labelContent...)

	if project.Status.ResourceVersion == util.GetMD5Hash(string(allContent[:])) && project.Status.Phase != auth.ProjectStatusError {
		return nil
	}
	// sync tke
	if needSyncTke() {
		err = r.syncTkeProject(project)
		if err != nil {
			return
		}

		// add tke finalizer
		if !contains(project.Finalizers, finalizer) {
			project.Finalizers = append(project.Finalizers, finalizer)
			err = r.updateProject(project)
			if err != nil {
				return
			}
		}
	}

	// sync namespace with same name of project
	err = r.syncProjectNamespace(project)
	if err != nil {
		return
	}

	// sync project binding and project quota
	err = r.syncProjectClusters(project)
	if err != nil {
		return
	}

	// check default namespace existence
	_, err = r.getProjectNamespace(project)
	if err != nil {
		return
	}

	var statusChanged, labelChanged bool
	if project.Status.Phase != auth.ProjectStatusActive {
		statusChanged = true
		project.Status.Phase = auth.ProjectStatusActive
	}

	label, exist := project.Labels[constant.LabelProject]
	if !exist || label != project.Name {
		labelChanged = true
		project.Labels[constant.LabelProject] = project.Name
	}

	if statusChanged {
		err = r.updateProjectStatus(project)
		if err != nil {
			return err
		}
	}
	if labelChanged {
		err = r.updateProject(project)
		if err != nil {
			return err
		}
	}

	log.Info("finished reconcile project", "name", request.Name)
	return nil
}

// newNamespace creates a new Namespace for a Project resource. It also sets
// the appropriate OwnerReferences on the resource so handleObject can discover
// the Project resource that 'owns' it.
func newNamespace(project *authv1.Project) *corev1.Namespace {
	labels := map[string]string{
		constant.LabelProject: project.Name,
	}
	// service mesh injection
	//if val, ok := serviceMesh(project); ok {
	//labels[auth.LabelIstioInjectionKey] = val
	//}

	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   project.Name,
			Labels: labels,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(project, schema.GroupVersionKind{
					Group:   authv1.SchemeGroupVersion.Group,
					Version: authv1.SchemeGroupVersion.Version,
					Kind:    "Project",
				}),
			},
		},
	}
}

// newProjectBinding creates a new ProjectBinding for a Project resource. It also sets
// the appropriate OwnerReferences on the resource so handleObject can discover
// the Project resource that 'owns' it.
func newProjectBinding(project *authv1.Project, cluster *authv1.ProjectClusters) *authv1.ProjectBinding {
	labels := map[string]string{
		constant.LabelProject: project.Name,
		constant.LabelCluster: cluster.Name,
	}

	return &authv1.ProjectBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:   fmt.Sprintf("%s-%s", project.Name, cluster.Name),
			Labels: labels,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(project, schema.GroupVersionKind{
					Group:   authv1.SchemeGroupVersion.Group,
					Version: authv1.SchemeGroupVersion.Version,
					Kind:    "Project",
				}),
			},
		},
	}
}

// newProjectQuota creates a new ProjectQuota for a Project resource.
// ProjectQuota maybe on another cluster, then ownerReference is not set.
func newProjectQuota(project *authv1.Project, cluster *authv1.ProjectClusters) *authv1.ProjectQuota {
	labels := map[string]string{
		constant.LabelProject:     project.Name,
		constant.LabelClusterName: cluster.Name,
		constant.LabelClusterType: cluster.Type,
	}

	quota := &authv1.ProjectQuota{
		ObjectMeta: metav1.ObjectMeta{
			Name:   project.Name,
			Labels: labels,
		},
	}

	if len(cluster.Quota) != 0 {
		quota.Spec = corev1.ResourceQuotaSpec{
			Hard: cluster.Quota,
		}
	}

	return quota
}

// updateProject update the project without status
func (r *ReconcileProject) updateProject(proj *authv1.Project) (err error) {
	// proj must be a copy of the original and modified
	return r.Update(context.TODO(), proj)
}

// updateProjectStatus update the status subresource of project
func (r *ReconcileProject) updateProjectStatus(proj *authv1.Project) (err error) {
	// proj must be a copy of the original and modified
	return r.Status().Update(context.TODO(), proj)
}

// deleteProjectClusters will handle deletion of projectbinding and projectquota of project
func (r *ReconcileProject) deleteProjectClusters(projectName string) (err error) {
	log.Info("Start delete related ProjectQuota/ProjectBinding for project", "name", projectName)
	// record if any error occurs

	// list bindings belong to project on global cluster
	bindings, err := r.listProjectBindings(projectName)
	if err != nil {
		log.Error(err, "delete project clusters, list project bindings of project", "projectName", projectName)
		return
	}

	// get the ProjectBinding object
	for _, binding := range bindings.Items {
		err = r.deleteProjectBinding(&binding, false)
		if err != nil {
			return
		}
	}

	return err
}

func (r *ReconcileProject) setNamespaceForceDeleteAnnotation(name string) error {
	log.Info("setNamespaceForceDeleteAnnotation", "name", name)
	namespace := &corev1.Namespace{}
	getNamespaceErr := r.Get(context.TODO(), types.NamespacedName{Name: name}, namespace)
	log.Info("setNamespaceForceDeleteAnnotation get ns", "name", name, "error", getNamespaceErr)

	if getNamespaceErr == nil {
		if len(namespace.Annotations) == 0 {
			namespace.Annotations = map[string]string{}
		}

		var changed bool
		forceDelete := namespace.Annotations[constant.AnnotationForceDelete]
		if forceDelete != "true" {
			changed = true
			namespace.Annotations[constant.AnnotationForceDelete] = "true"
		}

		if changed {
			err := r.Update(context.TODO(), namespace)
			log.Info("setNamespaceForceDeleteAnnotation update namespace", "name", name, "error", err)
			if err != nil {
				return err
			}
		}
	} else if !errors.IsNotFound(getNamespaceErr) {
		return getNamespaceErr
	}

	return nil
}

// namespaceSetDefaults make sure that Labels are initialized
func namespaceSetDefaults(ns *corev1.Namespace) *corev1.Namespace {
	if len(ns.Labels) == 0 {
		ns.Labels = map[string]string{}
	}
	return ns
}

// syncProjectNamespace will ensure a ns with same name of project exist and in Active status
func (r *ReconcileProject) syncProjectNamespace(project *authv1.Project) error {
	// get the namespace object
	namespace, getNamespaceErr := r.getProjectNamespace(project)

	if getNamespaceErr == nil {
		if namespace.Status.Phase == "Terminating" {
			//wait namespace delete
			log.Info("namespace exist, but in terminating status, will create new one", "name", project.Name)
			return r.createNamespace(project)
		} else {
			var labelChanged bool
			projectName, hasLabel := namespace.Labels[constant.LabelProject]
			if !hasLabel || projectName != project.Name {
				labelChanged = true
			}

			if labelChanged {
				// ensure projectl label exist
				namespace.Labels[constant.LabelProject] = project.Name
				err := r.Update(context.TODO(), namespace)
				if err != nil {
					return err
				}
			}

			//project exist && namespace exist,this is nomal case,safe leave.
			log.Info("namespace exist, and in active status", "name", project.Name)
		}
	} else if getNamespaceErr != nil && errors.IsNotFound(getNamespaceErr) {
		//project exist but namespace not found.In this case,need create namespace named the same as project
		log.Info("namespace does not exist, will create new one", "name", project.Name)
		return r.createNamespace(project)
	}

	return nil
}

// getProjectNamespace will get ns with same name with Project on global
func (r *ReconcileProject) getProjectNamespace(project *authv1.Project) (*corev1.Namespace, error) {
	// get the namespace object
	namespace := &corev1.Namespace{}
	getNamespaceErr := r.Get(context.TODO(), types.NamespacedName{Name: project.Name}, namespace)

	namespace = namespaceSetDefaults(namespace)

	return namespace, getNamespaceErr
}

// createNamespace will create ns with same name with Project on global and the ownerReference is set.
func (r *ReconcileProject) createNamespace(project *authv1.Project) error {
	// if --tke=true, then creation of namespace is handled by tke business-apiserver
	if needSyncTke() {
		return nil
	}
	namespace := newNamespace(project)
	createNamespaceErr := r.Create(context.TODO(), namespace)

	//create success
	if createNamespaceErr == nil {
		log.Info("Namespace created successfully", "name", namespace.Name)
	} else if !errors.IsAlreadyExists(createNamespaceErr) {
		log.Error(createNamespaceErr, "Namespace create error", "name", project.Name)
		return createNamespaceErr
	}
	return nil
}

// deleteProjectNamespaces delete namespace resources on specified cluster
func (r *ReconcileProject) deleteProjectNamespaces(projectName, clusterName string, keepResourceOrNot bool) (err error) {
	log.Info("Start delete namespaces", "projectName", projectName, "clusterName", clusterName)
	clientset, err := r.getK8sClientset(clusterName)
	if err != nil {
		log.Error(err, "Delete namespaces under project on cluster", "projectName", projectName, "clusterName", clusterName)
		return err
	}

	err = r.deleteFederatedNamespace(clusterName, projectName, keepResourceOrNot)
	if err != nil {
		return err
	}

	list, err := clientset.CoreV1().Namespaces().List(metav1.ListOptions{
		LabelSelector: fmt.Sprintf("%s=%s", constant.LabelProject, projectName),
	})
	if err != nil {
		log.Error(err, "Delete namespaces under project on cluster", "projectName", projectName, "clusterName", clusterName)
		return err
	}

	for _, namespace := range list.Items {
		// if --tke=true, then deletion of the default namespace is handled by tke business-apiserver
		if namespace.Name == projectName && needSyncTke() {
			continue
		}
		if keepResourceOrNot {
			if _, hasProjectLabel := namespace.Labels[constant.LabelProject]; hasProjectLabel {
				delete(namespace.Labels, constant.LabelProject)
			}
			if _, error := clientset.CoreV1().Namespaces().Update(&namespace); error != nil {
				log.Error(err, "can't update namespace ", "projectName", projectName, "clusterName", clusterName)
				return error
			}
		} else {
			err = clientset.CoreV1().Namespaces().Delete(namespace.Name, &metav1.DeleteOptions{})
			// delete the default namespace maybe raise PermissionDenied, when and only when
			// remove global from project, should not delete the default namespace
			if err != nil && !(errors.IsNotFound(err) || namespace.Name == projectName) {
				log.Error(err, "Delete namespaces under project on cluster", "projectName", projectName, "clusterName", clusterName, "namespaceName", namespace.Name)
				return err
			}
		}

	}

	log.Info("Finished delete namespaces", "projectName", projectName, "clusterName", clusterName)
	return nil
}

func (r *ReconcileProject) deleteFederatedNamespace(clusterName string, projectName string, keepResourceOrNot bool) error {
	clusterRegistry, err := r.getCluster(clusterName)
	if err != nil {
		log.Error(err, "Delete namespaces under project on cluster ", "projectName", projectName, "clusterName", clusterName)
		return err
	}
	federationHostName, hasLabel := clusterRegistry.Labels[constant.FederationHostName]
	federatedNamespaceList := &kubefed.FederatedNamespaceList{}
	if hasLabel && federationHostName == clusterName {
		labelSelector, err := labels.Parse(fmt.Sprintf("%s=%s", constant.LabelProject, projectName))
		if err != nil {
			return err
		}

		listOptions := &client.ListOptions{
			LabelSelector: labelSelector,
		}
		clusterClient, error := r.getClusterClient(federationHostName)
		if error != nil {
			log.Error(err, "Delete namespaces under project on cluster, get cluster client error ", "projectName", projectName, "clusterName", clusterName)
			return error
		}
		if err := clusterClient.List(context.TODO(), federatedNamespaceList, listOptions); err != nil {
			log.Error(err, "can't get federatednamespaces ", "projectName", projectName, "clusterName", clusterName)
			return err
		}
		for _, federatedNamespace := range federatedNamespaceList.Items {
			if keepResourceOrNot {
				if _, hasProjectLabel := federatedNamespace.Labels[constant.LabelProject]; hasProjectLabel {
					delete(federatedNamespace.Labels, constant.LabelProject)
				}
				if error := clusterClient.Update(context.TODO(), &federatedNamespace); error != nil {
					log.Error(err, "can't update federatednamespaces ", "projectName", projectName, "clusterName", clusterName)
					return error
				}
			}
		}
	}
	return nil
}

// getK8sClientset will construct a K8s client to access objects on given cluster with clusterName
func (r *ReconcileProject) getK8sClientset(clusterName string) (*kubernetes.Clientset, error) {
	config, err := r.getClusterConfig(clusterName)
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return clientset, nil
}

// syncProjectClusters will ensure project binding and project quota in correct state on associated clusters
func (r *ReconcileProject) syncProjectClusters(project *authv1.Project) (err error) {
	log.Info("Start sync clusters for project", "name", project.Name)
	projectName := project.Name

	// list bindings belong to project on global cluster
	bindings, err := r.listProjectBindings(projectName)
	if err != nil {
		log.Error(err, "list project bindings of project", "projectName", projectName)
		return
	}

	// store current available clusters
	curClusters := make(map[string]*authv1.ProjectClusters)
	for _, cluster := range project.Spec.Clusters {
		curClusters[cluster.Name] = cluster
	}
	log.Info("syncProjectClusters", "curClusters", curClusters)

	// store cluster with correct state
	syncedCluster := make(map[string]bool)

	var clusterErr error
	for _, item := range bindings.Items {
		binding := item.DeepCopy()
		binding.SetDefaults()
		clusterName, hasLabel := binding.Labels[constant.LabelCluster]
		if !hasLabel {
			log.Info("project binding does not have a cluster label, will delete it", "name", binding.Name)
			clusterErr = r.deleteProjectBinding(binding, project.Spec.ClusterDeletePolicy == authv1.Retain)
			if clusterErr != nil {
				err = clusterErr
				continue
			}
		}

		cluster, shouldRemain := curClusters[clusterName]
		log.Info("syncProjectClusters", "shouldRedmain", shouldRemain, "clusterName", clusterName)
		if !shouldRemain {
			log.Info("syncProjectClusters projectbinding exist but current clusters in project does not include specifed cluster", "clusterName", clusterName)
			clusterErr = r.deleteProjectBinding(binding, project.Spec.ClusterDeletePolicy == authv1.Retain)
			if clusterErr != nil {
				err = clusterErr
				continue
			}
		} else {
			log.Info("syncProjectClusters projectbinding exist and current clusters in project also include specifed cluster", "clusterName", clusterName)
			syncedCluster[clusterName] = true
			clusterErr = r.syncProjectBinding(project, cluster)
			if clusterErr != nil {
				err = clusterErr
				continue
			}
		}

	}
	log.Info("syncProjectClusters", "syncedClusters", syncedCluster)

	// sync new added clusters to correct state
	for _, clusterItem := range project.Spec.Clusters {
		cluster := clusterItem.DeepCopy()
		_, isOld := syncedCluster[cluster.Name]
		if !isOld {
			clusterErr = r.syncProjectCluster(project, cluster)
			if clusterErr != nil {
				err = clusterErr
				continue
			}
		}
	}

	log.Info("Finished sync clusters for project", "name", project.Name)
	return err
}

// syncProjectCluster will ensure project binding and project quota in correct state for specified cluster
func (r *ReconcileProject) syncProjectCluster(project *authv1.Project, cluster *authv1.ProjectClusters) error {
	log.Info("Start syncProjectCluster", "projectName", project.Name, "clusterName", cluster.Name)
	err := r.syncProjectBinding(project, cluster)
	if err != nil {
		return err
	}
	log.Info("Finished syncProjectCluster", "projectName", project.Name, "clusterName", cluster.Name)
	return nil
}

// syncProjectBinding ensure project binding in currect state for specified cluster
func (r *ReconcileProject) syncProjectBinding(project *authv1.Project, cluster *authv1.ProjectClusters) (err error) {
	log.Info("Start syncProjectBinding", "projectName", project.Name, "clusterName", cluster.Name)

	combinedNames := fmt.Sprintf("%s-%s", project.Name, cluster.Name)
	// get the ProjectBinding object
	binding := &authv1.ProjectBinding{}
	getBindingErr := r.Get(context.TODO(), types.NamespacedName{Name: combinedNames}, binding)

	if getBindingErr != nil && errors.IsNotFound(getBindingErr) {
		//project exist but project binding not found. In this case,need create project binding
		log.Info("project binding not exist on global, will create it", "name", combinedNames)
		r.createProjectBinding(newProjectBinding(project, cluster))
		return r.createProjectQuota(project, cluster)
	} else if getBindingErr == nil {
		var labelChanged bool
		binding.SetDefaults()
		_, hasLabel := binding.Labels[constant.LabelProject]
		if !hasLabel {
			labelChanged = true
			binding.Labels[constant.LabelProject] = project.Name
		}
		_, hasLabel = binding.Labels[constant.LabelCluster]
		if !hasLabel {
			labelChanged = true
			binding.Labels[constant.LabelCluster] = cluster.Name
		}

		if labelChanged {
			err = r.Update(context.TODO(), binding)
			if err != nil {
				return
			}
		}
	}

	// projectbinding and projectquota will always paired together
	err = r.syncProjectQuota(project, cluster)
	if err != nil {
		return
	}
	log.Info("Finished syncProjectBinding", "projectName", project.Name, "clusterName", cluster.Name)
	return nil
}

// syncProjectQuota ensure project quota on specified cluster exist and in correct state, will request remote cluster
func (r *ReconcileProject) syncProjectQuota(project *authv1.Project, cluster *authv1.ProjectClusters) error {
	log.Info("Start syncProjectQuota", "projectName", project.Name, "clusterName", cluster.Name)

	quota, getQuotaErr := r.getProjectQuota(project.Name, cluster.Name)

	if getQuotaErr != nil {
		if errors.IsNotFound(getQuotaErr) {
			//project exist but project quota not found. In this case, need create project quota
			log.Info("project quota not found, will create on cluster", "quotaName", project.Name, "clusterName", cluster.Name)
			return r.createProjectQuota(project, cluster)
		} else {
			log.Error(getQuotaErr, "get project quota", "quotaName", project.Name)
			return getQuotaErr
		}
	} else {
		var quotaChanged bool
		if !reflect.DeepEqual(quota.Spec.Hard, cluster.Quota) {
			quotaChanged = true
			if len(cluster.Quota) != 0 {
				quota.Spec.Hard = cluster.Quota
			} else {
				quota.Spec.Hard = corev1.ResourceList{}
			}
			log.Info("project quota exist, but quota is invalid will sync on cluster", "quotaNam", project.Name, "clusterName", cluster.Name)
		}

		var labelChanged bool
		quota.SetDefaults()
		projectName, hasLabel := quota.Labels[constant.LabelProject]
		if !hasLabel || projectName != project.Name {
			labelChanged = true
			quota.Labels[constant.LabelProject] = project.Name
		}
		clusterName, hasLabel := quota.Labels[constant.LabelClusterName]
		if !hasLabel || clusterName != cluster.Name {
			labelChanged = true
			quota.Labels[constant.LabelClusterName] = cluster.Name
		}

		if quotaChanged || labelChanged {
			return r.updateProjectQuota(project, cluster, quota)
		}

	}

	log.Info("Finished syncProjectQuota", "projectName", project.Name, "clusterName", cluster.Name)

	return nil
}

// listProjectBindings create ProjectBinding resource on k8s
func (r *ReconcileProject) listProjectBindings(projectName string) (*authv1.ProjectBindingList, error) {
	// list bindings belong to project on global cluster
	bindings := &authv1.ProjectBindingList{}
	labelSelector, err := labels.Parse(fmt.Sprintf("%s=%s", constant.LabelProject, projectName))
	if err != nil {
		return nil, err
	}

	listOpts := &client.ListOptions{
		LabelSelector: labelSelector,
	}

	err = r.List(context.TODO(), bindings, listOpts)
	if err != nil {
		log.Error(err, "list project bindings of project", "projectName", projectName)
		return nil, err
	}

	return bindings, nil
}

// createProjectBinding create ProjectBinding resource on k8s
func (r *ReconcileProject) createProjectBinding(binding *authv1.ProjectBinding) (err error) {
	log.Info("ProjectBinding does not exist. Will create now", "name", binding.Name)

	err = r.Create(context.TODO(), binding)

	//create success
	if err == nil || errors.IsAlreadyExists(err) {
		log.Info("ProjectBinding create successful", "name", binding.Name)
	} else {
		log.Error(err, "create ProjectBinding", "name", binding.Name)
		return
	}
	return
}

// deleteProjectBinding will also delete the related ProjectQuota on remote cluster if any exist
func (r *ReconcileProject) deleteProjectBinding(binding *authv1.ProjectBinding, keepResourceOrNot bool) (err error) {
	binding.SetDefaults()
	projectName, hasLabel := binding.Labels[constant.LabelProject]
	if !hasLabel {
		log.Info("project binding does not have a project label, will delete it", "name", binding.Name)
		return r.Delete(context.TODO(), binding)
	}
	clusterName, hasLabel := binding.Labels[constant.LabelCluster]
	if !hasLabel {
		log.Info("project binding does not have a cluster label, will delete it", "name", binding.Name)
		return r.Delete(context.TODO(), binding)
	}

	log.Info("ProjectBinding will be deleted on cluster", "name", binding.Name, "clusterName", clusterName)

	// if binding with incorrect labels then quota will not handled in this
	err = r.deleteProjectQuota(projectName, clusterName)
	if err != nil {
		return
	}

	// delete ns under specified cluster based on label
	err = r.deleteProjectNamespaces(projectName, clusterName, keepResourceOrNot)
	if err != nil {
		return
	}

	return r.Delete(context.TODO(), binding)
}

// getCluster retrieve the clusterregistry from global
func (r *ReconcileProject) getCluster(clusterName string) (*clusterv1alpha1.Cluster, error) {
	cluster := &clusterv1alpha1.Cluster{}
	leaderElectionNamespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: leaderElectionNamespace, Name: clusterName}, cluster)
	return cluster, err
}

// getClusterConfig will construct a rest.Config for given cluster with clusterName
func (r *ReconcileProject) getClusterConfig(clusterName string) (*rest.Config, error) {
	cluster, err := r.getCluster(clusterName)
	if err != nil {
		log.Error(err, "Failed to get cluster", "clusterName", clusterName)
		return nil, err
	}
	_, token, err := cluster.GetClusterNameAndToken2(r.Client)
	if err != nil {
		log.Error(err, "Failed to parse token from cluster", "clusterName", clusterName)
		return nil, err
	}
	config := rest.Config{
		Host:        apis.GetErebusEndpoint() + "/kubernetes/" + clusterName,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
		Timeout: time.Duration(apis.ApiTimeout) * time.Second,
	}
	return &config, nil
}

// getClusterClient will construct a AutuV1Client to access objects on given cluster with clusterName
func (r *ReconcileProject) getClusterClient(clusterName string) (client.Client, error) {
	config, err := r.getClusterConfig(clusterName)
	if config == nil {
		return nil, err
	}
	client, err := client.New(config, client.Options{})
	if err != nil {
		return nil, err
	}
	return client, nil
}

// getProjectQuota will retrieve ProjectQuota object from remote cluster with clusterName
func (r *ReconcileProject) getProjectQuota(projectName, clusterName string) (*authv1.ProjectQuota, error) {
	client, err := r.getClusterClient(clusterName)
	if err != nil {
		return nil, err
	}

	quota := &authv1.ProjectQuota{}
	err = client.Get(context.TODO(), types.NamespacedName{Name: projectName}, quota)

	return quota, err
}

// createProjectQuota create ProjectQuota resource on specified cluster
func (r *ReconcileProject) createProjectQuota(project *authv1.Project, cluster *authv1.ProjectClusters) (err error) {
	quota := newProjectQuota(project, cluster)
	log.Info("ProjectQuota does not exist on cluster. Will create now", "quotaName", quota.Name, "clusterName", cluster.Name)

	client, err := r.getClusterClient(cluster.Name)
	if err != nil {
		return
	}

	err = client.Create(context.TODO(), quota)

	//create success
	if err == nil || errors.IsAlreadyExists(err) {
		log.Info("ProjectQuota created successfully on cluster", "quotaName", quota.Name, "clusterName", cluster.Name)
	} else {
		log.Error(err, "ProjectQuota on cluster create error", "quotaName", quota.Name, "clusterName", cluster.Name)
		return
	}

	// update projectquota status
	quota.InitStatus()
	err = client.Status().Update(context.TODO(), quota)

	//update success
	if err == nil || errors.IsAlreadyExists(err) {
		log.Info("ProjectQuota created and updated status successfully on cluster", "quotaName", quota.Name, "clusterName", cluster.Name)
	} else {
		log.Error(err, "ProjectQuota on cluster created successfully, but update status error", "quotaName", quota.Name, "clusterName", cluster.Name)
		return
	}

	// add cluster finalizer
	finalizer := fmt.Sprintf("%s=%s", constant.LabelProject, project.Name)
	return r.createClusterFinalizer(cluster.Name, finalizer)
}

// updateProjectQuota update ProjectQuota resource on specified cluster
func (r *ReconcileProject) updateProjectQuota(project *authv1.Project, cluster *authv1.ProjectClusters, quota *authv1.ProjectQuota) (err error) {
	log.Info("ProjectQuota exist. Will be updated on cluster", "quotaName", quota.Name, "clusterName", cluster.Name)

	client, err := r.getClusterClient(cluster.Name)
	if err != nil {
		return
	}

	err = client.Update(context.TODO(), quota)

	//update success
	if err == nil {
		log.Info("ProjectQuota updated successfully on cluster", "quotaName", quota.Name, "clusterName", cluster.Name)
	} else {
		log.Error(err, "ProjectQuota on cluster update error", "quotaName", quota.Name, "clusterName", cluster.Name)
	}
	return
}

// deleteProjectQuota delete ProjectQuota resource on specified cluster
func (r *ReconcileProject) deleteProjectQuota(projectName, clusterName string) (err error) {
	log.Info("ProjectQuota will be deleted on cluster", "quotaName", projectName, "clusterName", clusterName)
	client, err := r.getClusterClient(clusterName)
	if err != nil {
		return err
	}

	err = client.Delete(context.TODO(), &authv1.ProjectQuota{
		ObjectMeta: metav1.ObjectMeta{
			Name: projectName,
		},
	})

	//delete success
	if err == nil || errors.IsNotFound(err) {
		log.Info("ProjectQuota deleted successfully on cluster", "quotaName", projectName, "clusterName", clusterName)
	} else {
		log.Error(err, "ProjectQuota on cluster delete error", "quotaName", projectName, "clusterName", clusterName)
		return
	}

	// remove finalizer from cluster
	finalizer := fmt.Sprintf("%s=%s", constant.LabelProject, projectName)
	err = r.deleteClusterFinalizer(clusterName, finalizer)

	return
}

// createClusterFinalizer add finalizer on specified cluster
func (r *ReconcileProject) createClusterFinalizer(clusterName, finalizer string) (err error) {
	finalizer = formatFinalizer(finalizer)
	log.Info("Add finalizer to cluster", "finalizer", finalizer, "clusterName", clusterName)

	cluster, err := r.getCluster(clusterName)
	if err != nil {
		log.Error(err, "Failed to retrieve cluster %v when adding finalizer(%v) with err: %v", clusterName, finalizer, err)
		return
	}

	if contains(cluster.Finalizers, finalizer) {
		log.Info("cluster has already has the finalizer", "finalizer", finalizer, "clusterName", clusterName)
		return
	}

	cluster.Finalizers = append(cluster.Finalizers, finalizer)

	err = r.Update(context.TODO(), cluster)

	//delete success
	if err == nil {
		log.Info("Add finalizer to cluster success", "finalizer", finalizer, "clusterName", clusterName)
	} else {
		log.Error(err, "Failed to update cluster for adding finalizer", "finalizer", finalizer, "clusterName", clusterName)
	}

	return
}

// deleteClusterFinalizer remove finalizer on specified cluster
func (r *ReconcileProject) deleteClusterFinalizer(clusterName, finalizer string) (err error) {
	finalizer = formatFinalizer(finalizer)
	log.Info("Remove finalizer from cluster", "finalizer", finalizer, "clusterName", clusterName)

	cluster, err := r.getCluster(clusterName)
	if err != nil {
		log.Error(err, "Failed to retrieve cluster when removing finalizer", "finalizer", finalizer, "clusterName", clusterName)
		return err
	}

	if !contains(cluster.Finalizers, finalizer) {
		return
	}

	cluster.Finalizers = removeItem(cluster.Finalizers, finalizer)

	err = r.Update(context.TODO(), cluster)

	//delete success
	if err == nil {
		log.Info("Remove finalizer on cluster success", "finalizer", finalizer, "clusterName", clusterName)
	} else {
		log.Error(err, "Failed to update cluster for removing finalizer", "finalizer", finalizer, "clusterName", clusterName)
	}

	return
}

func formatFinalizer(source string) (target string) {
	return strings.Replace(strings.Replace(source, "/", ".", -1), "=", ".", -1)
}

func contains(items []string, target string) bool {
	for _, item := range items {
		if item == target {
			return true
		}
	}

	return false
}

func removeItem(items []string, target string) (result []string) {
	for _, item := range items {
		if item != target {
			result = append(result, item)
		}
	}

	return result
}
