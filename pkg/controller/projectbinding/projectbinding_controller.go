/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package projectbinding

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/workqueue"
	"sigs.k8s.io/controller-runtime/pkg/event"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("projectbinding-controller")
var projectbindingsDict = make(map[string]map[string]string, 0)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new ProjectBinding Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.ProjectBinding) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.ProjectBinding))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileProjectBinding{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("projectbinding-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to ProjectBinding
	err = c.Watch(&source.Kind{Type: &authv1.ProjectBinding{}}, &handler.Funcs{
		CreateFunc: func(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "CreateEvent received with no metadata", "event", evt)
				return
			}
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
		DeleteFunc: func(evt event.DeleteEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "DeleteEvent received with no metadata", "event", evt)
				return
			}
			// set projectbinding dict
			labels := evt.Meta.GetLabels()
			if len(labels) > 0 {
				_, isProjectOk := labels[rc.LabelProject]
				_, isClusterOk := labels[rc.LabelCluster]
				if isProjectOk && isClusterOk {
					projectbindingsDict[evt.Meta.GetName()] = labels
				}
			}

			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
	})

	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileProjectBinding{}

// ReconcileProjectBinding reconciles a ProjectBinding object
type ReconcileProjectBinding struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a ProjectBinding object and makes changes based on the state read
// and what is in the ProjectBinding.Spec
func (r *ReconcileProjectBinding) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the ProjectBinding instance
	instance := &authv1.ProjectBinding{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	log.Info("ReconcileProjectBinding", "projectbinding", instance, "err", err)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Info("delete projectbinding", "projectbindingsDict", projectbindingsDict)
			labels, ok := projectbindingsDict[request.Name]
			if !ok {
				return reconcile.Result{}, nil
			}
			binding := &authv1.ProjectBinding{}
			binding.Name = request.Name
			binding.SetLabels(labels)
			r.DeleteUserBindingsByProjectBinding(binding)
			delete(projectbindingsDict, request.Name)
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// get cluster of project
	syncClusters := make([]string, 0)
	if cluster, ok := instance.Labels[rc.LabelCluster]; ok && len(cluster) > 0 {
		syncClusters = append(syncClusters, cluster)
	}

	project := ""
	if name, ok := instance.Labels[rc.LabelProject]; ok && len(name) > 0 {
		project = name
	}

	// get global userbindings of project
	userbindingList, err := apis.GetGlobalUserBindingList(r.Client, project)
	if err != nil {
		log.Error(err, "getGlobalUserbindings", "project", project)
		return reconcile.Result{}, nil
	}

	apis.SyncClusters(r.Client, syncClusters, userbindingList, func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		userbindingList, ok := obj.(*authv1.UserBindingList)
		if !ok {
			log.Error(fmt.Errorf("UserBindingList convert fail"), "ReconcileProjectBinding")
			return
		}
		for _, item := range userbindingList.Items {
			instance := item.InstanceForBusinessCluster()
			if err := clusterClient.Create(context.TODO(), instance.DeepCopyObject()); err != nil && !errors.IsAlreadyExists(err) {
				log.Error(err, "sync userbinding to project cluster", "cluster", clusterName, "project", instance.ProjectName())
			}
		}
	})

	return reconcile.Result{}, nil
}

// 过滤出global需要删除的userbindings，通过userbindings_controller统一删除
func (r *ReconcileProjectBinding) DeleteUserBindingsByProjectBinding(projectBinding *authv1.ProjectBinding) {
	cluster, ok := projectBinding.Labels[rc.LabelCluster]
	if !ok {
		log.Error(fmt.Errorf("cluster label not exists"), "DeleteUserBindingsOfProjectFromCluster", "name", projectBinding.Name)
		return
	}

	apis.SyncClusters(r.Client, []string{cluster}, projectBinding, func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		projectBinding, ok := obj.(*authv1.ProjectBinding)
		if !ok {
			log.Error(fmt.Errorf("projectBinding convert fail"), "DeleteUserBindingsOfProjectFromCluster", "name", projectBinding.Name, "cluster", clusterName)
			return
		}
		project, ok := projectBinding.Labels[rc.LabelProject]
		if !ok {
			log.Error(fmt.Errorf("project label not exists"), "DeleteUserBindingsOfProjectFromCluster", "name", projectBinding.Name, "cluster", clusterName)
			return
		}
		userBindingList := &authv1.UserBindingList{}
		labelSelector, _ := labels.Parse(fmt.Sprintf("%s,%s=%s", rc.LabelProject, rc.LabelProject, project))
		opts := &client.ListOptions{
			LabelSelector: labelSelector,
		}
		err := clusterClient.List(context.TODO(), userBindingList, opts)
		if err != nil {
			log.Error(err, "list unbinding userbindings in projectbinding", "name", projectBinding.Name, "cluster", clusterName)
			return
		}
		for _, item := range userBindingList.Items {
			// 不对global资源做同步操作
			if item.CurrentCluster() == rc.ClusterGlobal {
				continue
			}
			if err := clusterClient.Delete(context.TODO(), &item); err != nil && errors.IsNotFound(err) {
				log.Error(err, "DeleteUserBindingsByProjectBinding", "name", projectBinding.Name, "cluster", clusterName)
			}
		}
	})
}
