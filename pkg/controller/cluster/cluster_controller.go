/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cluster

import (
	"context"
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/types"

	"k8s.io/client-go/util/workqueue"
	"sigs.k8s.io/controller-runtime/pkg/event"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"

	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	apiextension "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	syncClusterMaxRetryTimes = 10
	syscRetry2Secounds       = 2
	syscRetry30Secounds      = 30
)

var log = logf.Log.WithName("cluster-controller")
var syncRetryTimes = make(map[string]int, 0)

// Add creates a new Cluster Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.Cluster) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.Cluster))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileCluster{
		Client: mgr.GetClient(),
		scheme: mgr.GetScheme(),
	}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("cluster-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Cluster
	err = c.Watch(&source.Kind{Type: &clusterv1alpha1.Cluster{}}, &handler.Funcs{
		CreateFunc: func(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "CreateEvent received with no metadata", "event", evt)
				return
			}
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
	})
	if err != nil {
		return err
	}
	return nil
}

var _ reconcile.Reconciler = &ReconcileCluster{}

// ReconcileCluster reconciles a Cluster object
type ReconcileCluster struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a Cluster object and makes changes based on the state read
func (r *ReconcileCluster) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the Cluster instance
	instance := &clusterv1alpha1.Cluster{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	//retryTimes := r.getSyncClusterRetryTimes(instance.Name)
	log.Info("ReconcileCluster", "name", request.NamespacedName, "err", err)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}
	// check cluster status
	for _, condition := range instance.Status.Conditions {
		if condition.Type == clusterv1alpha1.NotAccessible && condition.Status == corev1.ConditionTrue {
			log.Info("Cluster status NotAccessible is true", "cluster", instance.Name)
			return reconcile.Result{}, nil
		}
	}

	//if retryTimes >= syncClusterMaxRetryTimes {
	//	log.Error(fmt.Errorf("Sync cluster failure"), "cluster", instance.Name)
	//	r.resetSyncClusterRetryTimes(instance.Name)
	//	return reconcile.Result{}, nil
	//}

	// check crd exists and retry
	clusterClient, err := r.GetClusterClient(instance)
	if err != nil {
		//r.incrSyncClusterRetryTimes(instance.Name, err)
		log.Info("ReconcileCluster retry", "msg", "get cluster client")
		return reconcile.Result{RequeueAfter: time.Second * syscRetry2Secounds}, err
	}
	_, err = clusterClient.ApiextensionsV1beta1().CustomResourceDefinitions().Get("userbindings.auth.alauda.io", metav1.GetOptions{})
	if err != nil && errors.IsNotFound(err) {
		//r.incrSyncClusterRetryTimes(instance.Name, err)
		log.Info("ReconcileCluster retry", "msg", "check cluster userbinding crd")
		return reconcile.Result{RequeueAfter: time.Second * syscRetry30Secounds}, err
	}
	//r.resetSyncClusterRetryTimes(instance.Name)

	// target cluster
	syncClusters := []string{instance.Name}

	// sync clusterroles
	clusterroleList, err := apis.GetGlobalClusterRoles(r.Client)
	if err != nil {
		log.Error(err, "GetGlobalClusterRoles")
		return reconcile.Result{}, err
	}
	apis.SyncClusters(r.Client, syncClusters, clusterroleList, func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		clusterroleList, ok := obj.(*rbacv1.ClusterRoleList)
		if !ok {
			log.Error(fmt.Errorf("ClusterRoleList convert fail"), "ReconcileCluster")
			return
		}
		for _, item := range clusterroleList.Items {
			instance := apis.ClusterRoleForBusinessCluster(&item)
			instance.ResourceVersion = ""
			if apis.IsGlobalClusterRole(instance) || !apis.IsPlatformGenerateRole(instance) {
				continue
			}
			if err := clusterClient.Create(context.TODO(), instance.DeepCopyObject()); err != nil && !errors.IsAlreadyExists(err) {
				log.Error(err, "sync custerroles to business cluster", "cluster", clusterName, "name", instance.Name)
			}
		}
	})

	// get global userbindings of platform
	userbindingList, err := apis.GetGlobalUserBindingList(r.Client, "")
	if err != nil {
		log.Error(err, "GetGlobalUserBindingList")
		return reconcile.Result{}, err
	}
	apis.SyncClusters(r.Client, syncClusters, userbindingList, func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		userbindingList, ok := obj.(*authv1.UserBindingList)
		if !ok {
			log.Error(fmt.Errorf("UserBindingList convert fail"), "ReconcileCluster")
			return
		}
		for _, item := range userbindingList.Items {
			instance := item.InstanceForBusinessCluster()
			if err := clusterClient.Create(context.TODO(), instance.DeepCopyObject()); err != nil && !errors.IsAlreadyExists(err) {
				log.Error(err, "sync userbinding to business cluster", "cluster", clusterName, "name", instance.Name)
			}
		}
	})
	return reconcile.Result{}, nil
}

//func (r *ReconcileCluster) getSyncClusterRetryTimes(cluster string) int {
//	times, ok := syncRetryTimes[cluster]
//	if !ok {
//		r.resetSyncClusterRetryTimes(cluster)
//		times = 0
//	}
//	return times
//}

//func (r *ReconcileCluster) resetSyncClusterRetryTimes(cluster string) {
//	syncRetryTimes[cluster] = 0
//}

//func (r *ReconcileCluster) incrSyncClusterRetryTimes(cluster string, err error) {
//	_, ok := syncRetryTimes[cluster]
//	if !ok {
//		r.resetSyncClusterRetryTimes(cluster)
//	}
//	log.Error(err, "incrSyncClusterRetryTimes", "cluster", cluster)
//	syncRetryTimes[cluster] += 1
//}

func (r *ReconcileCluster) GetClusterClient(instance *clusterv1alpha1.Cluster) (*apiextension.Clientset, error) {
	var (
		token string
		err   error
	)
	_, token, err = instance.GetClusterNameAndToken2(r.Client)
	if err != nil {
		return nil, err
	}
	return apis.NewExtenionClientForCluster(instance.Name, token)
}
