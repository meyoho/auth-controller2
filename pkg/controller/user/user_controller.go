/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package user

import (
	"context"
	"fmt"

	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"github.com/btcsuite/btcutil/base58"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("user-controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new User Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.User) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.User))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileUser{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("user-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to User
	err = c.Watch(&source.Kind{Type: &authv1.User{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileUser{}

// ReconcileUser reconciles a User object
type ReconcileUser struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a User object and makes changes based on the state read
// and what is in the User.Spec
func (r *ReconcileUser) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the User instance
	instance := &authv1.User{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// delete user's userbindings in global cluster
			go r.DeleteUserBindingsByUser(request.Name)
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// create groups
	go r.CreateGroups(instance.Spec.Groups)

	// delete group relationship that does not exist
	go r.DeleteGroupBinding(instance)

	// create group relationships
	r.CreateGroupBindings(instance)

	return reconcile.Result{}, nil
}

func (r *ReconcileUser) CreateGroups(groups []string) {
	// group clear up by
	for _, groupName := range groups {
		if len(groupName) == 0 {
			continue
		}
		name := util.GetMD5Hash(groupName)
		group := &authv1.Group{}
		group.SetName(name)
		group.SetAnnotations(map[string]string{
			constant.AnnotationDisplayName: groupName,
		})
		err := r.Get(context.TODO(), types.NamespacedName{Name: group.Name}, &authv1.Group{})
		if err != nil && errors.IsNotFound(err) {
			err = r.Create(context.TODO(), group)
			if err != nil && !errors.IsAlreadyExists(err) {
				log.Error(err, "Creating CreateGroupBindings", "name", group.Name)
			}
		} else if err != nil {
			log.Error(err, "Creating CreateGroups")
		}
	}
}

func (r *ReconcileUser) DeleteGroupBinding(user *authv1.User) {
	labelSelector, _ := labels.Parse(constant.LabelUserEmail + "=" + user.Name)
	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	list := &authv1.GroupBindingList{}
	if err := r.List(context.TODO(), list, opts); err != nil {
		return
	}
	for _, groupbinding := range list.Items {
		// delete if new groups not in groups
		if !util.StringInSlice(groupbinding.Annotations[constant.AnnotationDisplayName], user.Spec.Groups) {
			r.Delete(context.TODO(), &groupbinding, &client.DeleteOptions{})
		}
	}
}

func (r *ReconcileUser) CreateGroupBindings(user *authv1.User) {

	for _, groupName := range user.Spec.Groups {
		if len(groupName) == 0 {
			continue
		}
		go func(groupName string) {
			groupNameMd5 := util.GetMD5Hash(groupName)
			groupNameBase58 := base58.Encode([]byte(groupName))
			name := user.Name + "-" + groupNameMd5
			groupbinding := &authv1.GroupBinding{}
			groupbinding.SetName(name)
			groupbinding.SetLabels(map[string]string{
				constant.LabelUserEmail:        user.Name,
				constant.LabelGroupName:        groupNameMd5,
				constant.LabelGroupDisplayName: groupNameBase58,
			})
			groupbinding.SetAnnotations(map[string]string{
				constant.AnnotationDisplayName: groupName,
			})

			if err := controllerutil.SetControllerReference(user, groupbinding, r.scheme); err != nil {
				log.Error(err, "CreateGroupBindings.SetControllerReference")
				return
			}

			// Check if the RoleBinding already exists
			err := r.Get(context.TODO(), types.NamespacedName{Name: groupbinding.Name}, &authv1.GroupBinding{})
			if err != nil && errors.IsNotFound(err) {

				err = r.Create(context.TODO(), groupbinding)
				if err != nil && !errors.IsAlreadyExists(err) {
					log.Error(err, "Creating CreateGroupBindings", "name", groupbinding.Name)
				}
				return
			} else if err != nil {
				log.Error(err, "Creating CreateGroupBindings", "DeleteUserBindingsByUsername", groupbinding.Name)
			}
		}(groupName)
	}
}

// delete user's userbindings
func (r *ReconcileUser) DeleteUserBindingsByUser(email string) error {
	if len(email) == 0 {
		return nil
	}
	userBindingList := &authv1.UserBindingList{}
	labelSelector, err := labels.Parse(fmt.Sprintf("%s,%s=%s", constant.LabelUserEmail, constant.LabelUserEmail, email))
	if err != nil {
		return err
	}

	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	err = r.Client.List(context.TODO(), userBindingList, opts)
	if err != nil {
		return err
	}
	if userBindingList != nil {
		for _, item := range userBindingList.Items {
			if err = r.Client.Delete(context.TODO(), &item); err != nil && !errors.IsNotFound(err) {
				log.Error(err, "DeleteUserBindingsByUser", "email", email)
			}
		}
	}
	return nil
}
