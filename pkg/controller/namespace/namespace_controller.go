/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package namespace

import (
	"context"
	"fmt"

	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/workqueue"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("namespace-controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Namespace Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileNamespace{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("namespace-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Namespace
	err = c.Watch(&source.Kind{Type: &corev1.Namespace{}}, &handler.Funcs{
		CreateFunc: func(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "CreateEvent received with no metadata", "event", evt)
				return
			}
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileNamespace{}

// ReconcileNamespace reconciles a Namespace object
type ReconcileNamespace struct {
	client.Client
	scheme *runtime.Scheme
}

func (r *ReconcileNamespace) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the Namespace instance
	log.Info("namespace-controller start", "NamespaceName:", request.NamespacedName)
	instance := &corev1.Namespace{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// check namespace
	if err := r.ValidateNamespace(instance); err != nil {
		return reconcile.Result{}, nil
	}

	// create project level rolebinding and clusterrolebinding
	list := r.ListProjecLeveltUserbindings(instance)
	for _, userbinding := range list {
		if err := userbinding.Validate(); err != nil {
			continue
		}

		// check user email
		if len(userbinding.UserEmail()) == 0 {
			continue
		}

		go func(userbinding authv1.UserBinding, namespace string) {
			// get project level clusterroles for userbinding
			clusterRoleList, err := r.GetClusterRoleList(userbinding.RoleName(), rc.RoleLevelProject, rc.RoleBindScopeNamespace, rc.RoleLabelPartCommon)
			log.Info("clusterRoleList:", "NamespaceName", request.NamespacedName)
			for _, v := range clusterRoleList.Items {
				log.Info("clusterRole", "Name: ", v.Name)
			}
			log.Info("--------------end")
			if err != nil || clusterRoleList == nil {
				log.Error(err, "list clusterroles", "namespace", namespace, "userbinding.name", userbinding.Name)
				return
			}

			for _, clusterRole := range clusterRoleList.Items {
				if err := apis.CreateRoleBinding(r.Client, r.scheme, &userbinding, namespace, clusterRole.Name); err != nil {
					log.Error(err, "create rolebinding for new ns", "namespace", namespace, "userbinding.name", userbinding.Name)
				}
			}
		}(userbinding, instance.Name)
	}
	log.Info("namespace-controller end", "NamespaceName:", request.NamespacedName)

	return reconcile.Result{}, nil
}

func (r *ReconcileNamespace) ListProjecLeveltUserbindings(ns *corev1.Namespace) []authv1.UserBinding {
	labelSelectorStr := fmt.Sprintf("%s,%s=%s,%s,%s=%s", rc.LabelProject, rc.LabelProject, ns.Labels[rc.LabelProject], rc.LabelRoleLevel, rc.LabelRoleLevel, rc.RoleLevelProject)
	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		return make([]authv1.UserBinding, 0)
	}

	listOptions := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	list := &authv1.UserBindingList{}
	err = r.List(context.TODO(), list, listOptions)
	if err != nil {
		return make([]authv1.UserBinding, 0)
	}
	return list.Items
}

func (r *ReconcileNamespace) GetClusterRoleList(relative, level, bindscope, bindnamespacepart string) (*rbacv1.ClusterRoleList, error) {
	list := &rbacv1.ClusterRoleList{}
	labelSelectorStr := fmt.Sprintf("%s=%s,%s=%s,%s=%s,%s=%s", rc.LabelRoleLevel, level, rc.LabelRoleRelative, relative, rc.LabelRolePart, bindnamespacepart, rc.LabelRoleBindScope, bindscope)
	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		return nil, err
	}

	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	if err := r.List(context.TODO(), list, opts); err != nil {
		return nil, err
	}
	return list, nil
}

func (r *ReconcileNamespace) ValidateNamespace(ns *corev1.Namespace) error {
	if project, ok := ns.Labels[rc.LabelProject]; project == "" || !ok {
		return fmt.Errorf("namespace project label is invalid")
	}
	return nil
}
