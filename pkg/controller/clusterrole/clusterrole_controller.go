/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clusterrole

import (
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	"context"
	"fmt"
	"k8s.io/client-go/util/workqueue"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/event"

	"k8s.io/apimachinery/pkg/types"

	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("clusterorle-controller")

var oldClusterRoleMap = map[string]*rbacv1.ClusterRole{}

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new ClusterRole Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.ClusterRole) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.ClusterRole))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileClusterRole{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("clusterrole-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to ClusterRole
	err = c.Watch(&source.Kind{Type: &rbacv1.ClusterRole{}}, &handler.Funcs{
		CreateFunc: func(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "CreateEvent received with no metadata", "event", evt)
				return
			}
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
		DeleteFunc: func(evt event.DeleteEvent, q workqueue.RateLimitingInterface) {
			if evt.Meta == nil {
				log.Error(nil, "DeleteEvent received with no metadata", "event", evt)
				return
			}
			oldClusterRole, _ := evt.Object.(*rbacv1.ClusterRole)
			oldClusterRoleMap[evt.Meta.GetName()] = oldClusterRole
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.Meta.GetName(),
				Namespace: evt.Meta.GetNamespace(),
			}})
		},
		UpdateFunc: func(evt event.UpdateEvent, q workqueue.RateLimitingInterface) {
			if evt.MetaNew == nil {
				log.Error(nil, "UpdateEvent received with no metadata", "event", evt)
				return
			}
			q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
				Name:      evt.MetaNew.GetName(),
				Namespace: evt.MetaNew.GetNamespace(),
			}})
		},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileClusterRole{}

// ReconcileClusterRole reconciles a ClusterRole object
type ReconcileClusterRole struct {
	client.Client
	scheme *runtime.Scheme
}

// roles sync
func (r *ReconcileClusterRole) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the ClusterRole instance
	instance := &rbacv1.ClusterRole{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			if clusterRole, ok := oldClusterRoleMap[request.NamespacedName.Name]; ok {
				r.SyncDeleteClusterRoleForBusinessClusters(clusterRole)
				delete(oldClusterRoleMap, request.NamespacedName.Name)
			}
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.

		// TODO add custom role delete
		return reconcile.Result{}, err
	}

	if !r.ValidClusterRole(instance) {
		return reconcile.Result{}, nil
	}

	//if apis.IsGlobalProjectNamespaceRole(instance) {
	//	return reconcile.Result{}, nil
	//}

	//if apis.IsSystemGlobalRole(instance) {
	//	return reconcile.Result{}, nil
	//}

	if !apis.IsGlobalClusterRoleAndPlatformGenerateRole(instance) {
		return reconcile.Result{}, nil
	}

	filterClusters := []string{}
	apis.SyncClusters(r.Client, filterClusters, instance.DeepCopyObject(), func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		role, ok := obj.(*rbacv1.ClusterRole)
		role = apis.ClusterRoleForBusinessCluster(role)
		if !ok {
			return
		}
		found := &rbacv1.ClusterRole{}
		err := clusterClient.Get(context.TODO(), types.NamespacedName{Name: role.Name}, found)
		if err != nil && errors.IsNotFound(err) {
			err = clusterClient.Create(context.TODO(), role)
			if err != nil && !errors.IsAlreadyExists(err) {
				log.Error(err, "Creating Clusterrole", "cluster", clusterName, "role", role.Name)
			}
			return
		} else if err != nil {
			log.Error(err, "Creating Clusterrole", "cluster", clusterName, "role", role.Name)
			return
		}

		// don't update global role and not platform generate role
		if apis.IsGlobalClusterRole(found) || !apis.IsPlatformGenerateRole(found) {
			return
		}
		// update
		if !reflect.DeepEqual(role.Rules, found.Rules) || !reflect.DeepEqual(role.Labels, found.Labels) {
			found.Rules = role.Rules
			found.Labels = role.Labels
			err = clusterClient.Update(context.TODO(), found)
			if err != nil {
				log.Error(err, "Updating ClusterRole", "name", role.Name)
			}
		}
	})

	return reconcile.Result{}, nil
}

func (r *ReconcileClusterRole) ValidClusterRole(role *rbacv1.ClusterRole) bool {
	if _, ok := role.Annotations[rc.AnnotationCurrentCluster]; !ok {
		return false
	}
	return true
}

func (r *ReconcileClusterRole) SyncDeleteClusterRoleForBusinessClusters(instance *rbacv1.ClusterRole) {
	if !r.ValidClusterRole(instance) {
		return
	}
	if !apis.IsGlobalClusterRoleAndPlatformGenerateRole(instance) {
		return
	}
	filterClusters := []string{}
	apis.SyncClusters(r.Client, filterClusters, instance.DeepCopyObject(), func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		role, ok := obj.(*rbacv1.ClusterRole)
		if !ok {
			return
		}
		found := &rbacv1.ClusterRole{}
		err := clusterClient.Get(context.TODO(), types.NamespacedName{Name: role.Name}, found)
		if err != nil && errors.IsNotFound(err) {
			log.Error(err, "Not Found Business ClusterRole", "cluster:", clusterName, "roleName:", found.Name)
			return
		} else if err != nil {
			log.Error(err, "Get Business ClusterRole Error", "cluster:", clusterName, "roleName:", found.Name)
			return
		}
		// don't delete global role and not platform generate role
		if apis.IsGlobalClusterRole(found) || !apis.IsPlatformGenerateRole(found) {
			return
		}
		err = clusterClient.Delete(context.TODO(), found)
		if err != nil {
			log.Error(err, "Delete Business ClusterRole Error ", "cluster:", clusterName, "roleName:", found.Name,
				"IsGlobalClusterRoleAndPlatformGenerateRole:", apis.ClusterRoleForBusinessCluster(found))
		} else {
			log.Info("Delete Business ClusterRole Success ", "cluster:", clusterName, "roleName:", found.Name,
				"IsGlobalClusterRoleAndPlatformGenerateRole:", apis.ClusterRoleForBusinessCluster(found))
		}
	})
}
