/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package functionresource

import (
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	"context"
	"fmt"

	authv1beta1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1beta1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("functionresource-controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new FunctionResource Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.FunctionResource) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.FunctionResource))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileFunctionResource{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("functionresource-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to FunctionResource
	err = c.Watch(&source.Kind{Type: &authv1beta1.FunctionResource{}}, &handler.EnqueueRequestForObject{})

	// TODO(user): Modify this to be the types you create
	// Uncomment watch a Deployment created by FunctionResource - change this for objects you create
	//err = c.Watch(&source.Kind{Type: &appsv1.Deployment{}}, &handler.EnqueueRequestForOwner{
	//	IsController: true,
	//	OwnerType:    &authv1beta1.FunctionResource{},
	//})
	//if err != nil {
	//	return err
	//}

	return nil
}

var _ reconcile.Reconciler = &ReconcileFunctionResource{}

// ReconcileFunctionResource reconciles a FunctionResource object
type ReconcileFunctionResource struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a FunctionResource object and makes changes based on the state read
// and what is in the FunctionResource.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=auth.alauda.io,resources=functionresources,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=functionresources/status,verbs=get;update;patch
func (r *ReconcileFunctionResource) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log.Info("funtionresources-controller start", "functionresource:", request.Name)
	r.UpdateRelatedRoleTemplates(request)
	return reconcile.Result{}, nil
}

func (r *ReconcileFunctionResource) UpdateRelatedRoleTemplates(request reconcile.Request) {
	list := &authv1beta1.RoleTemplateList{}
	labelSelectorStr := fmt.Sprintf(rc.LabelFunctionResourceRefSelector, request.NamespacedName.Name,
		request.NamespacedName.Name)
	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		return
	}

	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	if err := r.Client.List(context.TODO(), list, opts); err != nil {
		return
	}
	for _, roleTemplate := range list.Items {
		go func(roleTemplate authv1beta1.RoleTemplate) {
			//roleTemplate.UpdateClusterRoles(r.Client)
		}(roleTemplate)
	}
}
