/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package userbinding

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis/kubefed"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	"github.com/btcsuite/btcutil/base58"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("userbinding-controller")
var oldUserBindingMap = map[string]*authv1.UserBinding{}

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new UserBinding Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	if !apis.IsResourceEnabled(apis.UserBinding) {
		log.Info(fmt.Sprintf("%s controller not enabled!", apis.UserBinding))
		return nil
	}
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) *ReconcileUserBinding {
	return &ReconcileUserBinding{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r *ReconcileUserBinding) error {
	// Create a new controller
	c, err := controller.New("userbinding-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to UserBinding
	//err = c.Watch(&source.Kind{Type: &authv1.UserBinding{}}, &handler.Funcs{
	//	CreateFunc: func(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
	//		if evt.Meta == nil {
	//			log.Error(nil, "CreateEvent received with no metadata", "event", evt)
	//			return
	//		}
	//		q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
	//			Name:      evt.Meta.GetName(),
	//			Namespace: evt.Meta.GetNamespace(),
	//		}})
	//	},
	//	DeleteFunc: func(evt event.DeleteEvent, q workqueue.RateLimitingInterface) {
	//		if evt.Meta == nil {
	//			log.Error(nil, "DeleteEvent received with no metadata", "event", evt)
	//			return
	//		}
	//		q.Add(reconcile.Request{NamespacedName: types.NamespacedName{
	//			Name:      evt.Meta.GetName(),
	//			Namespace: evt.Meta.GetNamespace(),
	//		}})
	//	},
	//})
	pred := predicate.Funcs{
		CreateFunc: func(e event.CreateEvent) bool {
			r.Event = "create " + e.Meta.GetName()
			return true
		},
		DeleteFunc: func(e event.DeleteEvent) bool {
			r.Event = "delete " + e.Meta.GetName()
			oldUserBinding, _ := e.Object.(*authv1.UserBinding)
			oldUserBindingMap[e.Meta.GetName()] = oldUserBinding
			return true
		},
		UpdateFunc: func(e event.UpdateEvent) bool {
			r.Event = "update"
			oldUserBinding, ok1 := e.ObjectOld.(*authv1.UserBinding)
			newUserBinding, ok2 := e.ObjectNew.(*authv1.UserBinding)
			if ok1 && ok2 {
				if reflect.DeepEqual(oldUserBinding, newUserBinding) {
					return true
				}
			}
			return false
		},
		GenericFunc: func(e event.GenericEvent) bool {
			r.Event = "generic"
			return false
		},
	}
	err = c.Watch(&source.Kind{Type: &authv1.UserBinding{}}, &handler.EnqueueRequestForObject{}, pred)
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileUserBinding{}

// ReconcileUserBinding reconciles a UserBinding object
type ReconcileUserBinding struct {
	client.Client
	scheme *runtime.Scheme
	Event  string
}

// Reconcile reads that state of the cluster for a UserBinding object and makes changes based on the state read
// and what is in the UserBinding.Spec
func (r *ReconcileUserBinding) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the UserBinding instance
	filterClusters := []string{}
	instance := &authv1.UserBinding{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	log.Info("Reconcile", "event", r.Event, "name", request.NamespacedName)
	if err != nil {
		if errors.IsNotFound(err) {
			instance.Name = request.Name
			if userBinding, ok := oldUserBindingMap[instance.Name]; ok {
				if _, exsit := userBinding.Labels[rc.LabelFederated]; exsit {
					if clusterClient, err := r.getClusterClient(userBinding.Labels[rc.LabelCluster]); err != nil {
						log.Error(err, "get clusterClient err", "cluster name", userBinding.Labels[rc.LabelCluster])
					} else {
						federatedUserBinding := &kubefed.FederatedUserBinding{}
						err := clusterClient.Get(context.TODO(), request.NamespacedName, federatedUserBinding)
						if err != nil {
							log.Error(err, "get federateduserbinding err", "NamespacedName", request.NamespacedName)
							return reconcile.Result{}, nil
						}
						log.Info("federateduserbinding", "federateduserbinding", federatedUserBinding, "NamespacedName", request.NamespacedName)
						clusterClient.Delete(context.TODO(), federatedUserBinding)
					}

				}
				delete(oldUserBindingMap, instance.Name)
			}
			r.SyncDeleteUserBindingForBusinessClusters(filterClusters, instance)
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}
	// valid instance
	if err := instance.Validate(); err != nil {
		return reconcile.Result{}, nil
	}

	// Add extra data for global cluster
	if !instance.IsCurrentClusterExists() {
		r.addExtraDataForUserbinding(instance)
	}

	// create role bindings
	namespaces := r.GetUserBindingNamespaces(instance)
	var roleList *rbacv1.ClusterRoleList
	if roleList, err = getRelativeRolesByName(r.Client, instance.RoleName()); err != nil {
		log.Error(err, "getRelativeRolesByName", "roleName", instance.RoleName())
	}
	if roleList == nil {
		log.Error(fmt.Errorf("roleList is nil"), "getRelativeRolesByName", "roleName", instance.RoleName())
		return reconcile.Result{}, nil
	}
	for _, role := range roleList.Items {
		// create common role binding
		switch apis.GetRoleBindScope(&role) {
		case rc.RoleBindScopeCluster:
			if err := apis.CreateClusterRoleBinding(r.Client, r.scheme, instance, "", role.Name); err != nil && !errors.IsAlreadyExists(err) {
				log.Error(err, "CreateClusterRoleBinding", "roleName", role.Name)
			}
		case rc.RoleBindScopeNamespace:
			rolePart, ok := role.Labels[rc.LabelRolePart]
			if !ok {
				continue
			}
			switch rolePart {
			case rc.RoleLabelPartCommon:
				// business namespace binding
				for _, namespace := range namespaces {
					go func(ns string, role rbacv1.ClusterRole) {
						if err := apis.CreateRoleBinding(r.Client, r.scheme, instance, ns, role.Name); err != nil && !errors.IsAlreadyExists(err) {
							log.Error(err, "CreateRoleBinding", "roleName", role.Name)
						}
					}(namespace, role)
				}
			default:
				// special namespace binding
				var (
					isCreateRolebinding bool   = false
					ns                  string = ""
				)
				if apis.IsProjectSameNameRole(&role) {
					if apis.IsGlobalClusterRole(&role) {
						// Project with the same name
						isCreateRolebinding = true
						ns = instance.ProjectName()
					}
				} else if apis.IsSystemRole(&role) {
					// System role binding, eg. alauda-system
					if apis.IsSystemGlobalRole(&role) || apis.IsSystemBusinessRole(&role) {
						isCreateRolebinding = true
					}
					ns = util.GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
				} else {
					// Specific namespace, eg. kube-public
					if rolePart != "" {
						isCreateRolebinding = true
						ns = rolePart
					}
				}

				if isCreateRolebinding {
					if err := apis.CreateRoleBinding(r.Client, r.scheme, instance, ns, role.Name); err != nil && !errors.IsAlreadyExists(err) {
						log.Error(err, "create rolebinding", "roleName", role.Name, "rolePart", rolePart)
					}
				}
			}

		default:
			log.Error(fmt.Errorf("role.bindscope key does not exists"), "roleName", role.Name)
		}
	}

	// sync for business clusters
	if instance.IsNeedSync() || instance.ShouldFederateSync() {
		if instance.RoleLevel() == rc.RoleLevelProject {
			if filterClusters, err = getProjectClusters(r.Client, instance.ProjectName()); err != nil {
				log.Error(err, "getProjectClusters", "project", instance.ProjectName())
			}
		} else if instance.RoleLevel() == rc.RoleLevelNamespace {
			filterClusters = append(filterClusters, instance.NamespaceCluster())
		}

		go apis.SyncClusters(r.Client, filterClusters, instance.DeepCopyObject(), func(clusterClient client.Client, clusterName string, obj runtime.Object) {
			userbinding, ok := obj.(*authv1.UserBinding)
			if !ok {
				log.Error(fmt.Errorf("UserBinding convert fail"), "UserBinding")
				return
			}
			copy := userbinding.InstanceForBusinessCluster()
			if instance.ShouldFederateSync() {
				copy.APIVersion = "auth.alauda.io/v1"
				copy.Kind = "UserBinding"
				copy.Labels[rc.LabelFederated] = "false"
				copy.Labels[rc.LabelUserEmailBase58] = base58.Encode([]byte(instance.UserEmail()))
				userbindingObj, _ := json.Marshal(copy)
				userbindingUnstructed := unstructured.Unstructured{}
				userbindingUnstructed.UnmarshalJSON(userbindingObj)
				if copyUnstruucted, err := kubefed.FederateResources(&userbindingUnstructed); err != nil {
					log.Error(err, "federated userbinding error", copyUnstruucted)
					return
				} else {
					log.Info("federated userbinding is", "copyUnstruucted", copyUnstruucted)
					if err := clusterClient.Create(context.TODO(), copyUnstruucted); err != nil && !errors.IsAlreadyExists(err) {
						log.Error(err, "create instance for cluster", "cluster", clusterName, "instance", obj)
					}
				}
			} else {
				if err := clusterClient.Create(context.TODO(), copy.DeepCopyObject()); err != nil && !errors.IsAlreadyExists(err) {
					log.Error(err, "create instance for cluster", "cluster", clusterName, "instance", obj)
				}
			}
		})
	}

	return reconcile.Result{}, nil
}

// get namespace in project namespace or role namespace
func (r *ReconcileUserBinding) GetUserBindingNamespaces(instance *authv1.UserBinding) []string {
	var namespaces []string
	if instance.RoleLevel() == rc.RoleLevelPlatform {
		return namespaces
	}

	// namespace level
	if instance.RoleLevel() == rc.RoleLevelNamespace {
		err := r.Get(context.TODO(), types.NamespacedName{Name: instance.NamespaceName()}, &corev1.Namespace{})
		if err != nil && errors.IsNotFound(err) {
			return namespaces
		}
		namespaces = append(namespaces, instance.NamespaceName())
		return namespaces
	}

	// project level
	projectName := instance.ProjectName()
	labelSelector, err := labels.Parse(rc.LabelProject + "," + rc.LabelProject + "=" + projectName)
	if err != nil {
		return namespaces
	}
	listOptions := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	list := &corev1.NamespaceList{}
	if err := r.List(context.TODO(), list, listOptions); err != nil {
		return namespaces
	}
	for _, ns := range list.Items {
		namespaces = append(namespaces, ns.Name)
	}
	return namespaces
}

func (r *ReconcileUserBinding) addExtraDataForUserbinding(instance *authv1.UserBinding) {
	// TODO remove get user
	//user, err := authv1.GetUser(r.Client, instance.UserEmailName())
	//if err != nil {
	//	log.Info("user not exists", "userE", instance.UserEmailName())
	//	return
	//}

	// set email annotaion
	//instance.SetAnnotationUserEmail(user.Spec.Email)

	// set current-cluster annotaion
	if !instance.IsFederatedExsit() {
		instance.SetCurrentCluster(rc.ClusterGlobal)
	}

	// set userbindings ownerreference for project deleted
	owner := make([]metav1.OwnerReference, 0)
	if instance.RoleLevel() == rc.RoleLevelProject ||
		instance.RoleLevel() == rc.RoleLevelNamespace {
		project := &authv1.Project{}
		if err := r.Get(context.TODO(), types.NamespacedName{Name: instance.ProjectName()}, project); err == nil {
			owner = append(owner, metav1.OwnerReference{
				APIVersion: project.APIVersion,
				Kind:       project.Kind,
				Name:       project.Name,
				UID:        project.UID,
			})
		}
	}
	instance.SetOwnerReferences(owner)

	// update userbinding info
	if err := instance.Update(r.Client); err != nil {
		log.Error(err, "addExtraDataForUserbinding.UpdateUser")
	}
}

func getProjectClusters(cli client.Client, project string) (clusters []string, err error) {
	if len(project) == 0 {
		return
	}

	list := &authv1.ProjectBindingList{}
	labelSelector, err := labels.Parse(rc.LabelProject + "," + rc.LabelProject + "=" + project)
	if err != nil {
		return
	}
	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	if err = cli.List(context.TODO(), list, opts); err != nil {
		return
	}
	for _, item := range list.Items {
		if cluster, ok := item.Labels[rc.LabelCluster]; ok {
			clusters = append(clusters, cluster)
		}
	}
	return
}

func getRelativeRolesByName(cli client.Client, roleName string) (*rbacv1.ClusterRoleList, error) {
	list := &rbacv1.ClusterRoleList{}
	labelSelector, err := labels.Parse(rc.LabelRoleRelative + "," + rc.LabelRoleRelative + "=" + roleName)
	if err != nil {
		return nil, err
	}
	opts := &client.ListOptions{
		LabelSelector: labelSelector,
	}
	if err := cli.List(context.TODO(), list, opts); err != nil {
		return nil, err
	}
	return list, nil
}

func (r *ReconcileUserBinding) SyncDeleteUserBindingForBusinessClusters(filterClusters []string, instance *authv1.UserBinding) {
	go apis.SyncClusters(r.Client, filterClusters, instance, func(clusterClient client.Client, clusterName string, obj runtime.Object) {
		instance, ok := obj.(*authv1.UserBinding)
		if !ok {
			return
		}
		userbinding := &authv1.UserBinding{}
		if err := clusterClient.Get(context.TODO(), types.NamespacedName{Name: instance.Name}, userbinding); err != nil {
			return
		}
		if userbinding.CurrentCluster() == rc.ClusterGlobal {
			return
		}
		if err := clusterClient.Delete(context.TODO(), userbinding, &client.DeleteOptions{}); err != nil && !errors.IsNotFound(err) {
			log.Error(err, "delete userbinding", "name", instance.Name, "cluster", clusterName)
		}
	})
}

// getClusterConfig will construct a rest.Config for given cluster with clusterName
func (r *ReconcileUserBinding) getClusterConfig(clusterName string) (*rest.Config, error) {
	cluster, err := r.getCluster(clusterName)
	if err != nil {
		log.Error(err, "Failed to get cluster", "clusterName", clusterName)
		return nil, err
	}
	_, token, err := cluster.GetClusterNameAndToken2(r.Client)
	if err != nil {
		log.Error(err, "Failed to parse token from cluster", "clusterName", clusterName)
		return nil, err
	}
	config := rest.Config{
		Host:        apis.GetErebusEndpoint() + "/kubernetes/" + clusterName,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
		Timeout: time.Duration(apis.ApiTimeout) * time.Second,
	}
	return &config, nil
}

// getAuthClient will construct a AutuV1Client to access objects on given cluster with clusterName
func (r *ReconcileUserBinding) getClusterClient(clusterName string) (client.Client, error) {
	config, err := r.getClusterConfig(clusterName)
	if config == nil {
		return nil, err
	}
	client, err := client.New(config, client.Options{})
	if err != nil {
		return nil, err
	}
	return client, nil
}

// getCluster retrieve the clusterregistry from global
func (r *ReconcileUserBinding) getCluster(clusterName string) (*clusterv1alpha1.Cluster, error) {
	cluster := &clusterv1alpha1.Cluster{}
	leaderElectionNamespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", auth.NamespaceAlaudaSystem)
	err := r.Get(context.TODO(), types.NamespacedName{Namespace: leaderElectionNamespace, Name: clusterName}, cluster)
	return cluster, err
}
