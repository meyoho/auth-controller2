package utils

import (
	"context"
	"fmt"
	"log"
	"strings"

	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"

	"bitbucket.org/mathildetech/auth-controller2/cmd/util"
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	rc "bitbucket.org/mathildetech/auth-controller2/pkg/constant"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/klog"
)

const (
	ResProject      = "res:project"
	ResNamespace    = "res:ns"
	ResResourceName = "res:name"
	ResCluster      = "res:cluster"

	ErrNotEnoughPerms = "Not enough permissions"
)

type Permission struct {
	RoleName    string
	Actions     []string
	Constraints map[string]string
	Resource    schema.GroupResource
}

type SimpleAccessReview struct {
	Client client.Client
}

// IsInConstraint returns true if the permission constrains is equal or broader than the given
func (p *Permission) IsInConstraint(constraints map[string]string) bool {
	// no constrains, allows all
	if len(p.Constraints) == 0 {
		return true
	}
	if p.matchAllConstraints(constraints) {
		return true
	}

	return false
}

func (p *Permission) matchAllConstraints(constraints map[string]string) bool {
	//log.Printf("************p.Constraints: %+v, constraints: %+v", p.Constraints, constraints)

	for k, v := range p.Constraints {
		if val, ok := constraints[k]; !ok || v != val {
			//log.Printf(">>>>>>>constraints, k: %+v , val: %+v, v: %+v", k, val, v)
			return false
		}
	}
	return true
}

func Newconstraints(userBinding *authv1.UserBinding) map[string]string {
	constraints := map[string]string{}
	if len(userBinding.ProjectName()) > 0 {
		constraints[ResProject] = userBinding.ProjectName()
	}
	if len(userBinding.NamespaceName()) > 0 {
		constraints[ResNamespace] = userBinding.NamespaceName()
	}
	if len(userBinding.NamespaceCluster()) > 0 {
		constraints[ResCluster] = userBinding.NamespaceCluster()
	}
	return constraints
}

func NewPermission(userbinding *authv1.UserBinding, resource schema.GroupResource, actions []string,
	resourceName string) *Permission {
	constraints := Newconstraints(userbinding)
	if len(resourceName) > 0 {
		constraints[ResResourceName] = resourceName
	}

	return &Permission{
		RoleName:    userbinding.RoleName(),
		Actions:     actions,
		Constraints: constraints,
		Resource:    resource,
	}
}

func EmailToName(email string) string {
	if len(email) == 0 {
		return ""
	}
	name := email
	name = util.GetMD5Hash(name)
	name = strings.ToLower(name)
	return name
}

func (s *SimpleAccessReview) Verify(email string, action string, resource schema.GroupResource, constraints map[string]string) (bool, error) {
	actions, err := s.GetActions(email, resource, constraints)
	klog.Infof("Verify email: %v constraints: %v actions: %v", email, constraints, actions)
	if err != nil {
		return false, err
	}

	return s.HasAction(action, actions), nil
}

// hasAction - verifies if the string is in the slice
// action: action string e.g service:create
// allowed: slice of allowed actions e.g. service:create, service:update, etc..
func (s *SimpleAccessReview) HasAction(action string, allowed []string) bool {
	for _, a := range allowed {
		if a == "*" {
			return true
		}
		if a == action {
			return true
		}
	}
	return false
}

func (s *SimpleAccessReview) GetActions(email string, resource schema.GroupResource, constraints map[string]string) ([]string, error) {
	var (
		actionsMap = make(map[string]struct{})
		place      = struct{}{}
		actions    []string
	)

	if constraints == nil {
		constraints = map[string]string{}
	}

	userPerms, err := s.GetUserPermissions(email, resource)
	if err != nil {
		return actions, err
	}

	for _, p := range userPerms {
		log.Printf("GetActions.permission: %+v", *p)
		if p.IsInConstraint(constraints) {
			for _, a := range p.Actions {
				actionsMap[a] = place
			}
		}
	}

	actions = make([]string, 0)
	for k := range actionsMap {
		if k == "*" {
			actions = []string{"*"}
			break
		}
		actions = append(actions, k)
	}
	return actions, nil
}

func (s *SimpleAccessReview) GetUserPermissions(email string, resource schema.GroupResource) ([]*Permission, error) {
	var (
		permissions      = make([]*Permission, 0)
		emailMd5         = EmailToName(email)
		labelSelectorStr = fmt.Sprintf("%s,%s=%s", rc.LabelUserEmail, rc.LabelUserEmail, emailMd5)
	)

	labelSelector, err := labels.Parse(labelSelectorStr)
	if err != nil {
		return permissions, err
	}
	userBindingList := &authv1.UserBindingList{}
	err = s.Client.List(context.TODO(), userBindingList, &client.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		return permissions, err
	}
	cli, err := s.GetKubernetesClient()
	if err != nil {
		return permissions, err
	}
	for _, userbinding := range userBindingList.Items {
		ch := make(chan []*Permission)
		go func(chnl chan []*Permission) {
			perms, err := GetUserBindingPermissions(cli, &userbinding, resource)
			if err != nil {
			}
			chnl <- perms
			close(chnl)
		}(ch)
		for perms := range ch {
			permissions = append(permissions, perms...)
		}
	}
	return permissions, nil
}

func (s *SimpleAccessReview) GetKubernetesClient() (*kubernetes.Clientset, error) {
	cfg, err := config.GetConfig()
	if err != nil {
		return nil, err
	}
	cli, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	return cli, nil
}

func GetUserBindingPermissions(k8sClient *kubernetes.Clientset, userbinding *authv1.UserBinding,
	resource schema.GroupResource) ([]*Permission, error) {
	labelSelector := fmt.Sprintf("%s,%s=%s", rc.LabelRoleRelative, rc.LabelRoleRelative, userbinding.RoleName())
	opts := metav1.ListOptions{
		LabelSelector: labelSelector,
	}
	clusterRoleList, err := k8sClient.RbacV1().ClusterRoles().List(opts)
	if err != nil {
		return nil, err
	}
	permissions := make([]*Permission, 0)
	for _, clusterRole := range clusterRoleList.Items {
		ch := make(chan *Permission)
		go func(clusterRole *rbacv1.ClusterRole, userbinding *authv1.UserBinding, resource schema.GroupResource, chnl chan *Permission) {
			for _, rule := range clusterRole.Rules {
				// loop apigroup
				for _, group := range rule.APIGroups {

					if resource.Group != group && group != "*" {
						continue
					}

					// loop resources
					for _, res := range rule.Resources {

						if resource.Resource != res && res != "*" {
							continue
						}

						// loop resourceNames
						if len(rule.ResourceNames) > 0 {
							for _, resourceName := range rule.ResourceNames {

								chnl <- NewPermission(userbinding, resource, rule.Verbs, resourceName)
							}
						} else {
							chnl <- NewPermission(userbinding, resource, rule.Verbs, "")
						}

					} // end resources

				} // end apigroup
			}
			close(chnl)

		}(&clusterRole, userbinding, resource, ch)

		for perm := range ch {
			permissions = append(permissions, perm)
		}
	}
	return permissions, nil
}
