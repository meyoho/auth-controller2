package utils

import (
	"context"

	authutil "bitbucket.org/mathildetech/auth-controller2/cmd/util"
	"bitbucket.org/mathildetech/auth-controller2/pkg/apis"
	authapis "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"

	clusterv1alpha1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/clusterregistry/v1alpha1"
)

// getCluster retrieve the clusterregistry from global
func GetCluster(clusterName string, client client.Client) (*clusterv1alpha1.Cluster, error) {
	cluster := &clusterv1alpha1.Cluster{}
	leaderElectionNamespace := authutil.GetEnv("LEADER_ELECTION_NAMESPACE", authapis.NamespaceAlaudaSystem)
	err := client.Get(context.TODO(), types.NamespacedName{Namespace: leaderElectionNamespace, Name: clusterName}, cluster)
	return cluster, err
}

// GetClusterConfig will construct a rest.Config for given cluster with clusterName
func GetClusterConfig(clusterName string, client client.Client) (*rest.Config, error) {
	cluster, err := GetCluster(clusterName, client)
	if err != nil {
		return nil, err
	}
	_, token, err := cluster.GetClusterNameAndToken2(client)
	if err != nil {
		return nil, err
	}
	config := rest.Config{
		Host:        apis.GetErebusEndpoint() + "/kubernetes/" + clusterName,
		BearerToken: token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	return &config, nil
}

// GetClusterClient will construct a client to access objects on given cluster with clusterName
func GetClusterClient(clusterName string, cli client.Client) (client.Client, error) {
	config, err := GetClusterConfig(clusterName, cli)
	if config == nil {
		return nil, err
	}
	c, err := client.New(config, client.Options{})
	if err != nil {
		return nil, err
	}
	return c, nil
}
